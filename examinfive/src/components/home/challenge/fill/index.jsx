import React, { useState,useEffect } from "react";
import { Button, Input } from "antd";
import "../question/question.css";

const questionsData = [
  {
    id: 1,
    text: "视图函数必须接受一个___对象，必须返回一个___对象。",
    blanks: [
      { id: 1, correctAnswer: "请求" },
      { id: 2, correctAnswer: "响应" },
    ],
  },
  {
    id: 2,
    text: "ORM中常用的字段类型有___、___、___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "字符型" },
      { id: 2, correctAnswer: "整型" },
      { id: 3, correctAnswer: "浮点型" },
      { id: 4, correctAnswer: "日期型" },
      { id: 5, correctAnswer: "布尔型" },
    ],
  },
  {
    id: 3,
    text: "ORM中常用的字段属性有___、___、___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "主键(primary key)" },
      { id: 2, correctAnswer: "唯一(unique)" },
      { id: 3, correctAnswer: "默认值(default)" },
      { id: 4, correctAnswer: "外键(foreign key)" },
      { id: 5, correctAnswer: "索引(index)" },
    ],
  },
  {
    id: 4,
    text: "ORM生成数据表的命令：___ 和 ___。",
    blanks: [
      { id: 1, correctAnswer: "python manage.py makemigrations" },
      { id: 2, correctAnswer: "python manage.py migrate" },
    ],
  },
  {
    id: 5,
    text: "自动记录数据的添加时间的属性是 ___。",
    blanks: [{ id: 1, correctAnswer: "auto_now_add" }],
  },
  {
    id: 6,
    text: "存放CSS、JS静态文件的目录是 ___。",
    blanks: [{ id: 1, correctAnswer: "static" }],
  },
  {
    id: 7,
    text: "将字符串全部转换成大写的过滤器是 ___。",
    blanks: [{ id: 1, correctAnswer: "upper" }],
  },
  {
    id: 8,
    text: "Django的MySQL的模块是 ___。",
    blanks: [{ id: 1, correctAnswer: "django.db.backends.mysql" }],
  },
  {
    id: 9,
    text: "ORM删除记录调用 ___ 方法。",
    blanks: [{ id: 1, correctAnswer: "delete" }],
  },
  {
    id: 10,
    text: "Django中数据模型的关联关系有___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "一对一关系" },
      { id: 2, correctAnswer: "一对多关系" },
      { id: 3, correctAnswer: "多对多关系" },
    ],
  },
  {
    id: 11,
    text: "Django常用的中间件有___、___。",
    blanks: [
      { id: 1, correctAnswer: "CsrfViewMiddleware" },
      { id: 2, correctAnswer: "SessionMiddleware" },
    ],
  },
  {
    id: 12,
    text: "如果path函数不能满足精确匹配的要求，可以使用___函数。",
    blanks: [{ id: 1, correctAnswer: "re_path" }],
  },
  {
    id: 13,
    text: "视图函数要求必须接收一个___对象作为参数。",
    blanks: [{ id: 1, correctAnswer: "请求" }],
  },
  {
    id: 14,
    text: "{{my_list|first|upper}}的作用是___。",
    blanks: [{ id: 1, correctAnswer: "取列表的第一个元素，并将其转换为大写形式" }],
  },
  {
    id: 15,
    text: "redirect函数表示___。",
    blanks: [{ id: 1, correctAnswer: "重定向到另一个URL" }],
  },
  {
    id: 16,
    text: "Django MTV分别代表___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "模型(Model)" },
      { id: 2, correctAnswer: "视图(View)" },
      { id: 3, correctAnswer: "模板(Template)" },
    ],
  },
  {
    id: 17,
    text: "cookie特点是存放在___端。",
    blanks: [{ id: 1, correctAnswer: "客户端" }],
  },
  {
    id: 18,
    text: "{{ name|default:\"helloworld\" }},如果模板变量的值为空或者False,就显示___。",
    blanks: [{ id: 1, correctAnswer: "\"helloworld\"" }],
  },
  {
    id: 19,
    text: "Django的主要文件包括___、___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "settings.py" },
      { id: 2, correctAnswer: "urls.py" },
      { id: 3, correctAnswer: "views.py" },
      { id: 4, correctAnswer: "models.py" },
    ],
  },
  {
    id: 20,
    text: "ORM的意思是___对应的单词分别是___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "对象关系映射(Object Relational Mapping)" },
      { id: 2, correctAnswer: "对象" },
      { id: 3, correctAnswer: "关系" },
      { id: 4, correctAnswer: "映射" },
    ],
  },
  {
    id: 21,
    text: "写出5个数据库操作常用函数：___、___、___、___、___。",
    blanks: [
      { id: 1, correctAnswer: "增加记录" },
      { id: 2, correctAnswer: "删除记录" },
      { id: 3, correctAnswer: "修改记录" },
      { id: 4, correctAnswer: "查询记录" },
      { id: 5, correctAnswer: "过滤记录" },
    ],
  },
  {
    id: 22,
    text: "模型的一对一的模型名称是___。",
    blanks: [{ id: 1, correctAnswer: "OneToOneField" }],
  },
  {
    id: 23,
    text: "通过___命令启动项目并占用8080端口。",
    blanks: [{ id: 1, correctAnswer: "python manage.py runserver" }],
  },
  {
    id: 24,
    text: "python manage.py runserver 169.254.243.9:9090 可以同时指定___和___。",
    blanks: [
      { id: 1, correctAnswer: "IP地址" },
      { id: 2, correctAnswer: "端口号" },
    ],
  },
  {
    id: 25,
    text: "session的特点是存放在___端的。",
    blanks: [{ id: 1, correctAnswer: "服务端" }],
  },
];

const FillInBlank = ({ question, onAnswer }) => {
  const [answers, setAnswers] = useState(Array(question.blanks.length).fill(""));

  const handleAnswer = (blankIndex, value) => {
    const newAnswers = [...answers];
    newAnswers[blankIndex] = value;
    setAnswers(newAnswers);
  };

  return (
    <div>
      <div className="question">{question.id}.{question.text}</div>
      {question.blanks.map((blank, index) => (
        <div key={blank.id} className="blank">
          <Input
            value={answers[index]}
            onChange={(e) => handleAnswer(index, e.target.value)}
            placeholder={`填写第${index + 1}个空`}
          />
        </div>
      ))}
    </div>
  );
};

const Index = () => {
  const [showAnswers, setShowAnswers] = useState(false);
  const [timer, setTimer] = useState(60 * 60); // 60分钟

  useEffect(() => {
    const interval = setInterval(() => {
      if (timer > 0 && !showAnswers) {
        setTimer((prevTimer) => prevTimer - 1);
      } else {
        clearInterval(interval);
        setShowAnswers(true);
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [timer, showAnswers]);

  const handleShowAnswers = () => {
    setShowAnswers(true);
  };

  const handleRestart = () => {
    window.location.reload();
  };

  return (
    <div className="q-container">
      <div className="left-panel">
        {questionsData.map((question) => (
          <FillInBlank key={question.id} question={question} />
        ))}
      </div>
      <div className="right-panel">
        <div className="answerer-info">答题人信息</div>
        {showAnswers && (
          <div className="results">
            <h3>正确答案：</h3>
            {questionsData.map((question, index) => (
              <div key={question.id} className="result">
                <span>第{index + 1}题：</span>
                {question.blanks.map((blank) => (
                  <span key={blank.id}>{blank.correctAnswer}&nbsp;</span>
                ))}
              </div>
            ))}
          </div>
        )}
        <div className="timer">剩余时间: {Math.floor(timer / 60)}:{timer % 60 < 10 ? `0${timer % 60}` : timer % 60}</div>
        {!showAnswers ? (
          <Button onClick={handleShowAnswers} type="primary">提交答案</Button>
        ) : (
          <Button onClick={handleRestart} type="primary">重新答题</Button>
        )}
      </div>
    </div>
  );
};

export default Index;
