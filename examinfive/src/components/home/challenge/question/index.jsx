import React, { useState, useEffect } from "react";
import "./question.css";
import { Button } from "antd";
import { color } from "echarts";
const questionsData = [
  {
    id: 1,
    text: "React是什么？",
    options: [
      "A. 一个用于构建用户界面的JavaScript库",
      "B. 一种编程语言",
      "C. 一个数据库管理系统",
      "D. 一个CSS框架",
    ],
    correctAnswer: 0,
  },
  {
    id: 2,
    text: "以下哪个不是React的核心概念？",
    options: [
      "A. 组件",
      "B. 状态（State）",
      "C. 路由（Routing）",
      "D. 属性（Props）",
    ],
    correctAnswer: 2,
  },
  {
    id: 3,
    text: "Vue是一个什么样的库？",
    options: [
      "A. 用于构建用户界面的JavaScript库",
      "B. 一种编程语言",
      "C. 一个数据库管理系统",
      "D. 一个CSS框架",
    ],
    correctAnswer: 0,
  },
  {
    id: 4,
    text: "以下哪个不是Vue的核心概念？",
    options: [
      "A. 组件",
      "B. 状态（State）",
      "C. 路由（Routing）",
      "D. 属性（Props）",
    ],
    correctAnswer: 2,
  },
  {
    id: 5,
    text: "React中，如何向组件传递属性（props）？",
    options: [
      "A. 使用this.props传递",
      "B. 使用this.state传递",
      "C. 使用this.parent传递",
      "D. 使用this.children传递",
    ],
    correctAnswer: 0,
  },
  {
    id: 6,
    text: "Vue中，如何向组件传递属性？",
    options: [
      "A. 使用this.props传递",
      "B. 使用this.data传递",
      "C. 使用this.parent传递",
      "D. 使用this.children传递",
    ],
    correctAnswer: 0,
  },
  {
    id: 7,
    text: "React中，如何管理组件的状态？",
    options: [
      "A. 使用this.props管理",
      "B. 使用this.state管理",
      "C. 使用this.parent管理",
      "D. 使用this.children管理",
    ],
    correctAnswer: 1,
  },
  {
    id: 8,
    text: "Vue中，如何管理组件的状态？",
    options: [
      "A. 使用this.props管理",
      "B. 使用this.state管理",
      "C. 使用this.data管理",
      "D. 使用this.children管理",
    ],
    correctAnswer: 2,
  },
  {
    id: 9,
    text: "React中，如何定义一个函数组件？",
    options: [
      "A. 使用class关键字定义",
      "B. 使用function关键字定义",
      "C. 使用def关键字定义",
      "D. 使用fun关键字定义",
    ],
    correctAnswer: 2,
  },
  {
    id: 10,
    text: "Vue中，如何定义一个函数组件？",
    options: [
      "A. 使用class关键字定义",
      "B. 使用function关键字定义",
      "C. 使用def关键字定义",
      "D. 使用fun关键字定义",
    ],
    correctAnswer: 2,
  },
];

const Index = () => {
  const [selectedOptions, setSelectedOptions] = useState(
    Array(questionsData.length).fill(null)
  );
  const [showResults, setShowResults] = useState(false);
  const [answeredQuestions, setAnsweredQuestions] = useState(
    Array(questionsData.length).fill(false)
  );
  const [timer, setTimer] = useState(60 * 60); // 60分钟
  const [buttonText, setButtonText] = useState("提交试卷");
  useEffect(() => {
    const interval = setInterval(() => {
      if (timer > 0 && !showResults) {
        setTimer((prevTimer) => prevTimer - 1);
      } else {
        clearInterval(interval);
        setShowResults(true);
        setButtonText("重新答题");
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [timer, showResults]);

  const handleAnswer = (questionIndex, selectedAnswerIndex) => {
    const newSelectedOptions = [...selectedOptions];
    newSelectedOptions[questionIndex] = selectedAnswerIndex;
    setSelectedOptions(newSelectedOptions);

    const newAnsweredQuestions = [...answeredQuestions];
    newAnsweredQuestions[questionIndex] = true;
    setAnsweredQuestions(newAnsweredQuestions);
  };

  const handleSubmit = () => {
    setShowResults(true);
    setButtonText("重新答题");
  };
  const handleRestart = () => {
    window.location.reload();
  };
  return (
    <div className="q-container">
      <div className="left-panel">
        {questionsData.map((question, index) => (
          <div key={question.id}>
            <div className="question">
              {index + 1}.{question.text}
            </div>
            <div className="options">
              {question.options.map((option, optionIndex) => (
                <div
                  key={optionIndex}
                  className={`option ${
                    selectedOptions[index] === optionIndex ? "selected" : ""
                  }`}
                  onClick={() => handleAnswer(index, optionIndex)}
                >
                  {option}
                </div>
              ))}
            </div>
            {showResults && (
              <div className={`correct-answer ${showResults && selectedOptions[index] !== question.correctAnswer ? 'show' : ''}`}>
              正确答案：{String.fromCharCode(65 + question.correctAnswer)}
            </div>
            
            )}
          </div>
        ))}
      </div>
      <div className="right-panel">
        <div className="answerer-info">答题人信息</div>
        <div className="timer">
          剩余时间: {Math.floor(timer / 60)}:
          {timer % 60 < 10 ? `0${timer % 60}` : timer % 60}
        </div>
        <div className="question-list">
          {questionsData.map((question, index) => (
            <div
              key={index}
              className={`question-number 
            ${
              selectedOptions[index] !== null
                ? showResults &&
                  selectedOptions[index] === question.correctAnswer
                  ? "correct"
                  : "incorrect"
                : "unanswered"
            }`}
            >
              {index + 1}
            </div>
          ))}
        </div>
        <Button className="q-button" type="primary" onClick={showResults ? handleRestart : handleSubmit}>
          {buttonText}
        </Button>
      </div>
    </div>
  );
};

export default Index;
