import React from "react";
import "./challenge.css";
import { Card,Button } from "antd";
const { Meta } = Card;

export default function Challenge() {
  const goQuestion = () => {
    window.location.href = "/#/home/challenge/question";
  };
  const goFill = () => {
    window.location.href = "/#/home/challenge/fill";
  }
  return (
    <div className="container">
      <Card
        hoverable
        style={{
          width: 340,
          height: 550,
        }}
        cover={
          <img
            alt="example"
            src="/1.jpg"
            className="card-cover"
          />
        }
      >
        <Meta title="选择题" description="用户点击开始答题进入选择题自我挑战，进行考察个人学习情况" />
        <Button className="button" onClick={goQuestion}>
            立即答题
        </Button>
      </Card>
      <Card
        hoverable
        style={{
          width: 340,
          height: 550,
        }}
        cover={
          <img
            alt="example"
            src="/2.jpg"
            className="card-cover"
          />
        }
      >
        <Meta title="填空题" description="用户点击开始答题进入填空题自我挑战，进行考察个人学习情况" />
        <Button className="button" onClick={goFill}>
            立即答题
        </Button>
      </Card>
      <Card
        hoverable
        style={{
          width: 340,
          height: 550,
        }}
        cover={
          <img
            alt="example"
            src="/3.jpg"
            className="card-cover"
          />
        }
      >
        <Meta title="简答题" description="用户点击开始答题进入简答题自我挑战，进行考察个人学习情况" />
        <Button className="button" danger>
            维护中
        </Button>
      </Card>
      
    </div>
  );
}
