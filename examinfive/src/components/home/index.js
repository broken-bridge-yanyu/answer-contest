import React, { useState, useEffect } from 'react';
import { Layout, Menu, Button, Dropdown, Space } from 'antd';
import {
  UserOutlined,
  SettingOutlined,
  SyncOutlined,
  LoginOutlined,
} from '@ant-design/icons';
import './index.css'
import { Outlet, useNavigate, useLocation } from 'react-router-dom'
import { navListGet } from '../../service/index'
import '../../function/icon/home/navListHome/iconfont.css'

const { Header, Sider, Content, Footer } = Layout;
function getItem(label, key, icon, children, type) {
  return {
    label,
    key,
    icon,
    children,
    type,
  };
}

export default function Index() {
  const items = [
    {
      key: '1',
      label: (
        <button className='buttons' onClick={() => (Personalcenter())}><UserOutlined />个人中心</button>
      ),
    },
    {
      key: '2',
      label: (
        <button className='buttons'><SyncOutlined /> 清除缓存</button>
      ),
    },
    {
      key: '3',
      label: (
        <button className='buttons' onClick={(() => { quiT() })}><LoginOutlined />退出</button>
      ),
    },
  ];
  const navigate = useNavigate();
  const location = useLocation()
  const [User] = useState(JSON.parse(localStorage.getItem('user')) || {})
  const Personalcenter = () => {
    navigate('/personalcenter')
  }
  const quiT = () => {
    localStorage.removeItem('accessToken')
    localStorage.removeItem('userId')
    localStorage.removeItem('user')
    localStorage.removeItem('contestant')
    navigate('/')
  }
  //导航
  useEffect(() => {
    navReq()
  }, [])
  let [navList, setNavList] = useState([])
  const navReq = () => {
    navListGet('home/navigationBarList').then(res => {
      let { data } = res
      setNavList(data)
    })
  }
  //父级导航
  const iconFather = [
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-shouye'></span>,//首页
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-gongji'></span>,//参赛人员
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-icon_tuanduiyouxi_normal'></span>,//比赛页
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-rdshanshui'></span>,//自我挑战
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-zhinanzhen'></span>,//活动管理
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-huodong'></span>,//活动管理
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-shequfuwuzhan'></span>,//社区
    <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-quanxianguanli'></span>,//权限管理
  ]
  const itemss = () => {
    return navList.map((item, index) => {
      let childrenItems = null;
      if (item.children && item.children.length > 0) {
        childrenItems = item.children.map((ite, inde) => getItem(ite.navName, ite.path, <span style={{ display: 'block', fontSize: '20px' }} className='iconfontHome icon-V'></span>));
      }
      return getItem(item.navName, item.path, iconFather[index], childrenItems);
    });
  };

  const [collapseds, setCollapseds] = useState(false);
  const toggleCollapsed = () => {
    setCollapseds(!collapseds);
  };
  const onClick = (e) => {
    navigate(e.key)
  };
  return (
<<<<<<< HEAD
    <div>

    </div>
  )
=======
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapseds} onCollapse={toggleCollapsed} style={{ backgroundColor: '#fff' }}>
        <div className="logo" style={{ width: collapseds ? '100%' : '50%', height: collapseds ? '90px' : '100px', transition: 'width 0.3s', margin: 'auto', display: 'block' }}>
          <img src="/log.png" alt="" style={{ width: '100%', height: '100%' }} />
        </div>
        <div
          style={{
            width: '100%'
          }}
        >
          <Menu
            defaultSelectedKeys={[location.pathname]}
            onClick={onClick}
            mode="inline"
            theme="light"
            inlineCollapsed={collapseds}
            items={itemss()}
          />
        </div>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0, backgroundColor: 'rgb(230, 231, 233)', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Space wrap className='userBox' >
            <img src="/log.png" alt="" className='imguse' style={{ width: '50px' }} />
            <span className='userName'>{User.username}</span>
            <Dropdown
              menu={{
                items,
              }}
              placement="bottomRight"
            >
              <Button className='menus' ><SettingOutlined style={{ fontSize: '20px' }}></SettingOutlined></Button>
            </Dropdown>
          </Space>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            marginTop: '10px',
            backgroundColor: '#fff',
            padding: 10,
            minHeight: 280,
          }}
        >
          <Outlet></Outlet>
        </Content>
        <Footer style={{ padding: '1% 1%' }}><a className="bsn" href="https://beian.miit.gov.cn/">ICP备案号:冀ICP备2023043377号</a></Footer>
      </Layout>
    </Layout>
  );
>>>>>>> dev
}
