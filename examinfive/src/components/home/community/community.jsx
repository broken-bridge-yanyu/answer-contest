import React, { useState, useEffect } from 'react';
import { Space, Table, Button } from 'antd';
import { instance } from "../../../service/request"; // 请确保路径正确

const App = () => {
  const [data, setData] = useState([]);
  const [expandedRowKeys, setExpandedRowKeys] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await instance.get("/rob/roblist");
        // 假设 response.question2 是正确的数据路径，根据实际调整
        setData(response.question2); // 根据您的API响应结构调整
      } catch (error) {
        console.error('Fetching data failed', error);
      }
    };

    fetchData();
  }, []);

  const getTextPreview = (text) => {
    return `${text.substring(0, 50)}...`; // 根据需要调整字符数
  };

  const columns = [
    {
      title: '问题',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '答案',
      dataIndex: 'info',
      key: 'info',
      render: (info, record) => {
        const fullText = info[0]?.title; // 假设答案存储在info数组的第一个元素的title属性中
        if (!fullText) return null;
        return expandedRowKeys.includes(record._id) ? fullText : getTextPreview(fullText);
      },
    },
    {
      title: '操作',
      key: 'operation',
      render: (_, record) => (
        <Space size="middle">
          <Button onClick={() => handleExpand(!expandedRowKeys.includes(record._id), record)}>
            {expandedRowKeys.includes(record._id) ? '收起' : '展开'}
          </Button>
        </Space>
      ),
    },
  ];

  const handleExpand = (expanded, record) => {
    const keys = expanded ? [...expandedRowKeys, record._id] : expandedRowKeys.filter(key => key !== record._id);
    setExpandedRowKeys(keys);
  };

  return (
    <Table
      columns={columns}
      dataSource={data}
      rowKey="_id"
      expandedRowKeys={expandedRowKeys}
      onExpand={handleExpand}
      pagination={{ pageSize: 5 }} // 设置每页显示5条记录
    />
  );
};

export default App;
