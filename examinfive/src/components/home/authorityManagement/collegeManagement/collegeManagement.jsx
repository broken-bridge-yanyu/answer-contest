import React, { useEffect, useState } from 'react'
import './collegeManagement.css'
import { Button, Table, Input, message, Modal, Typography, Popconfirm } from 'antd';
import {
    PlusOutlined, DeleteOutlined, CheckOutlined, CloseOutlined
} from '@ant-design/icons';
import { GetCollegeApi, DeleteCollegeApi, PostCollegeApi } from '../../../../service/index'
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};
export default function CollegeManagement() {
    const [messageApi, contextHolder] = message.useMessage();
    let [college, setCollege] = useState([])
    useEffect(() => {
        CollegeList()
    }, [])
    //数据渲染
    const CollegeList = () => {
        GetCollegeApi('/personnel/collegeList').then(res => {
            let { code, msg, data } = res
            if (code === 200) {
                setCollege(data)
            } else {
                messageApi.open({
                    type: 'error',
                    content: msg,
                });
            }
        })
    }
    //学院添加
    const [showInputRow, setShowInputRow] = useState(false); // 控制显示输入行的状态
    const toggleInputRow = () => {
        setShowInputRow(!showInputRow); // 切换显示输入行的状态
    }
    let [collegeAdd, setCollegeAdd] = useState({
        collegeName: '',
        level: 0,
        c_id: ''
    })
    const [addChilder, serAddChilder] = useState('')
    const onChange = (e) => {
        setCollegeAdd({
            collegeName: e.target.value,
            level: 0,
            c_id: ''
        })
    };
    const [isModalOpen, setIsModalOpen] = useState(false);//模态框状态
    const msgs = (code, msg) => {
        if (code === 200) {
            messageApi.open({
                type: 'success',
                content: msg,
            });
            setCollegeAdd({
                collegeName: '',
                level: 0,
                c_id: ''
            })
        } else {
            messageApi.open({
                type: 'error',
                content: msg,
            });
        }
    }
    const inputAdd = (record) => {
        if (record) {
            setCollegeAdd({
                collegeName: record.collegeName,
                level: record.level + 1,
                c_id: record._id
            })
            setShowInputRow(false)
            setIsModalOpen(!isModalOpen)
        } else {
            PostCollegeApi('personnel/collegeAdd', collegeAdd).then(res => {
                let { code, msg } = res
                if (code === 200) {
                    msgs(code, msg)
                    setShowInputRow(!showInputRow)
                    CollegeList()
                } else {
                    msgs(code, msg)
                }
            })
        }
    }
    //子级添加
    const handleOk = () => {
        PostCollegeApi('personnel/collegeAdd', { collegeAdd, addChilder }).then(res => {
            let { code, msg } = res
            if (code == 200) {
                msgs(code, msg)
                serAddChilder('')
                setIsModalOpen(false);
                CollegeList()
            } else {
                msgs(code, msg)
            }
        })
    };
    const { confirm } = Modal;
    const handleCancel = () => {
        confirm({
            title: '确认取消',
            content: '您确定要取消吗？',
            okText: '确认',
            cancelText: '取消',
            onOk() {
                // 这里放置确认取消后的逻辑，或者直接关闭 Modal
                serAddChilder('')
                setCollegeAdd({
                    collegeName: '',
                    level: 0,
                    c_id: ''
                })
                setIsModalOpen(false);
            },
            onCancel() {
                serAddChilder('')
                setCollegeAdd({
                    collegeName: '',
                    level: 0,
                    c_id: ''
                })
                // 用户点击取消，无需进行任何操作
            },
        });
    };
    //修改数据
    const [editingInputValue, setEditingInputValue] = useState('');
    const [editingRow, setEditingRow] = useState(null);
    const handleRowClick = (record) => {//点击获取想要编辑得某一行得数据
        setEditingRow(record._id); // 设置当前正在编辑的行
        setEditingInputValue(record.collegeName); // 将该行的值填入 input 框
    };
    const handleInputChange = (e) => {//通过双向通信来实时更新input里面得值
        setEditingInputValue(e.target.value); // 更新 input 框的值
    };
    const handleSave = (record) => {
        PostCollegeApi(`personnel/collegeUpd/${record._id}`, { data: editingInputValue }).then(res => {
            let { code, msg } = res// 调用修改数据的 API
            if (code === 200) {
                msgs(code, msg)
                setEditingRow(null);// 设置 editingRow 为 null 表示编辑完成
                CollegeList();// 更新成功后重新获取学院列表
            } else {
                msgs(code, msg)
            }
        })
    };
    const renderColumns = (text, record) => {
        const isEditing = record._id === editingRow;
        return isEditing ? (
            <Input
                style={{ width: '200px' }}
                value={editingInputValue}
                onChange={handleInputChange}
                onPressEnter={() => handleSave(record)}
                onBlur={() => handleSave(record)}
            />
        ) : (
            <div onClick={() => handleRowClick(record)}>{text}</div>
        );
    };
    //删除
    const delCollege = (data) => {
        DeleteCollegeApi(`personnel/collegeDel/${data._id}`).then(res => {
            let { code, msg } = res
            if (code == 200) {
                msgs(code, msg)
                CollegeList()
            } else {
                msgs(code, msg)
            }
        })
    }
    const columns = [
        {
            title: '学院',
            dataIndex: 'collegeName',
            key: 'collegeName',
            width: 200,
            render: renderColumns,
        },
        {
            title: '操作',
            width: 200,
            render: (_, record) => (
                <div>
                    <Button onClick={() => inputAdd(record)} style={record.level == 2 ? { display: 'none' } : {}} type="primary" shape="circle" icon={<PlusOutlined />} />
                    <Button onClick={() => delCollege(record)} type="primary" danger shape="circle" icon={<DeleteOutlined />} />
                </div>
            )
        },
    ];
    return (
        <div id='College'>
            {contextHolder}
            <div className='CollegeBox'>
                <span className='title'>学院管理</span>
                <div className='tableBox'>
                    <div className='addBox'>
                        <Button disabled={showInputRow} onClick={toggleInputRow}>添加学院</Button>
                        <div style={showInputRow ? {} : { display: 'none' }} className='inputAdd'>
                            <div className='inputs'>
                                <Input showCount value={collegeAdd.collegeName} maxLength={20} onChange={onChange} />
                            </div>
                            <Button type="primary" onClick={() => inputAdd()} shape="circle" icon={<CheckOutlined />} />
                            <Button type="primary" onClick={() => (setShowInputRow(!showInputRow), setCollegeAdd({ collegeName: '', level: 0, c_id: '', superiors: '' }))} shape="circle" danger icon={<CloseOutlined />} />
                        </div>
                    </div>
                    <Table
                        scroll={{ y: 300 }}
                        columns={columns}
                        rowSelection={{
                            ...rowSelection,
                        }}
                        dataSource={college}
                        pagination={false}
                        rowKey={(record) => record._id}
                    />
                </div>
                <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <div className='addBox'>
                        <Typography.Title level={5}>学院：</Typography.Title>
                        <Input value={collegeAdd.collegeName} disabled placeholder="上一级" />
                        <Typography.Title level={5}>阶段/班级：</Typography.Title>
                        <Input value={addChilder} onChange={e => serAddChilder(e.target.value)} placeholder="请输入班级" />
                    </div>
                </Modal>
            </div>
        </div>
    )
}
