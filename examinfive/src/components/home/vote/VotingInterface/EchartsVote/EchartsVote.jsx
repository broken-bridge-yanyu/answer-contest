import React, { useEffect } from 'react';
import * as echarts from 'echarts';

export default function EchartsVote({ voteDatas }) {
    useEffect(() => {
        const chartDom = document.getElementById('main');
        const myChart = echarts.init(chartDom);
        let votes = [];
        let name = [];
        if (voteDatas && voteDatas[0] && voteDatas[0].contestant) {
            votes = voteDatas[0].contestant.map((item) => item.votes);
            name = voteDatas[0].contestant.map((item) => item.name);
        }
        let option = {
            xAxis: {
                max: 'dataMax',
            },
            yAxis: {
                type: 'category',
                data: name,
                inverse: true,
                animationDuration: 300,
                animationDurationUpdate: 300,
                axisLabel: {
                    show: true,//是否显示刻度标签
                    interval: 0,//设置标签的显示间隔
                    // rotate: 50,//设置标签的旋转角度
                    fontWeight: "bold",//设置标签字体的加粗
                    fontSize: 15//设置标签字体大小
                }
            },
            series: [
                {
                    realtimeSort: true,
                    name: 'X',
                    type: 'bar',
                    data: votes,
                    colorBy: "data",
                    label: {
                        show: true,
                        position: 'right',
                        valueAnimation: true,
                        fontWeight: "bold",//标签字体加粗
                        fontSize: 25,//标签字体大小
                    },
                },
            ],
            legend: {
                show: true,
            },
            tooltip: {
                trigger: "axis",//触发类型为坐标轴触发
                axisPointer: {
                    type: "shadow"//坐标轴指示器类型为阴影
                },
                formatter: function (params) {
                    let name = params[0].name; // 获取键名（名称）
                    let value = params[0].value; // 获取键值（数据）
                    return `团队/个人：${name}<br/>票数：${value}`;
                }
            },
            animationDuration: 0,
            animationDurationUpdate: 3000,
            animationEasing: 'linear',
            animationEasingUpdate: 'linear',

        };

        function run() {
            for (let i = 0; i < votes.length; ++i) {
                if (Math.random() > 0.9) {
                    votes[i] += Math.round(Math.random() * 2000);
                } else {
                    votes[i] += Math.round(Math.random() * 200);
                }
            }
            myChart.setOption({
                series: [
                    {
                        type: 'bar',
                        votes,
                    },
                ],
            });
        }
        const timer = setTimeout(function () {
            run();
        }, 0);
        const interval = setInterval(function () {
            run();
        }, 3000);

        myChart.setOption(option);
        return () => {
            clearTimeout(timer);
            clearInterval(interval);
            myChart.dispose(); // 销毁echarts实例
        };
    }, [voteDatas]);
    return (
        <div id="main" style={{ height: '100%', width: '100%' }} />
    )
}
