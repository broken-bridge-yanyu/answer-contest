import React, { useEffect, useState } from 'react'
import './index.css'
import EchartsVote from './EchartsVote/EchartsVote'
import { useSearchParams } from 'react-router-dom'
import ReturnTopBtn from '../../../../function/functionBox/vote/returnTopBtn/returnTopBtn'
let baseURL = 'http://localhost:7321/'
export default function Index() {
    const [searchParams] = useSearchParams();
    const id = searchParams.get("id")
    let [voteData, setVoteData] = useState([])
    useEffect(() => {
        // 1、创建一个SSE连接
        const source = new EventSource(`${baseURL}serverSSE/actionVote/${id}`)
        // 2、监听SSE事件，open事件，message事件，ping事件，error事件
        source.addEventListener('open', (e) => {
            console.log(e + '服务器已连接');
        })
        //打开SSE信息
        source.addEventListener('message', (e) => {
            const data = e.data ? JSON.parse(e.data) : null
            setVoteData(data)
            console.log(data, 123);
        })
        //接收SSE自定义事件
        // source.addEventListener('ping', (e) => {
        //     console.log('接收SSE自定义事件信息', e);
        // })
        source.addEventListener('error', (e) => {
            // @ts-ignore
            if (e.currentTarget.readyState === 0) {
                console.log('SSE连接已关闭');
            }
        })
        // 在窗口即将关闭时关闭SSE连接
        window.addEventListener('beforeunload', () => {
            source.close();
        });
        // const closeTimer = setTimeout(() => {
        //     source.close(); // 关闭 SSE 连接
        // }, 5 * 60 * 1000); // 五分钟，以毫秒为单位
        return () => {
            // 组件卸载时关闭SSE连接
            source.close();
            // clearTimeout(closeTimer);
            console.log('已经于SSE连接关闭');
        };
    }, [id])
    console.log(voteData, 123);
    return (
        <div className='votinginterfaceBox'>
            <div className='top'>
                <ReturnTopBtn pathNavigate={-1} />
            </div>
            <div className='buttom'>
                <EchartsVote voteDatas={voteData} />
            </div>
        </div>
    )
}
