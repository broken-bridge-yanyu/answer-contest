import React from 'react'
import './index.css'

export default function index({ data }) {
    return (
        <div key={data._id} className='users'>
            {/* <img className='img' src={`http://154.8.152.188:7321/${item.portrait}`} alt='' /> */}
            {/* 本地 */}
            <img className='img' src={`http://localhost:7321/${data.portrait}`} alt='' />
            <div className='userInfos'>
                <div className='userInfosTitle'>团队名称：{data.name}</div>
                <div className='state'>
                    状态：{data.state ? '已参赛' : '弃权'}
                </div>
                <div className='votes'>票数：{data.votes}</div>
            </div>
        </div>
    )
}
