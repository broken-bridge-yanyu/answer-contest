import React, { useEffect, useState } from 'react'
import './index.css'
import ChartBar from './chart/index'
import '../../../../function/icon/vote/returnTop/iconfont.css'
import { useSearchParams } from 'react-router-dom'
import ReturnTopBtn from '../../../../function/functionBox/vote/returnTopBtn/returnTopBtn'
import { GetVoteApi } from '../../../../service/index'
import Ranking from './ranking/index'
import UserInfo from './userInfo/index'

export default function Index() {
    const [flag, setFlag] = useState(false);
    function handleClick() {
        setFlag((prevFlag) => !prevFlag);
    }
    let [data, setData] = useState({})
    let [sortVote, setSortVote] = useState([])
    const [searchParams] = useSearchParams();
    const id = searchParams.get("id");
    useEffect(() => {
        const oneData = async () => {
            try {
                GetVoteApi(`vote/listParticipants/${id}`).then(res => {
                    let { code, data } = res
                    if (code === 200) {
                        setData(data[0])
                        let datas = data[0].contestant.sort((a, b) => (
                            b.votes - a.votes)
                        )
                        // 创建一个空数组
                        let topThreeContestants = [];
                        // 将前三条数据保存到空数组中
                        for (let i = 0; i < Math.min(3, datas.length); i++) {
                            topThreeContestants.push(datas[i]);
                        }
                        setSortVote(topThreeContestants)
                    };
                })
            } catch (error) {
                console.error(error);
            }
        }
        oneData()
    }, [id])
    return (
        <div className='singleQuery'>
            <div className='singleQueryBox'>
                <div className='singleQueryBoxTop'>
                    <ReturnTopBtn pathNavigate={'/home/vote'} />
                </div>
                <div className='singleQueryBoxButtom'>
                    {data && Object.keys(data).length > 0 && <ChartBar echartsData={data} />}
                </div>
                <button className='DetailsBtn' onClick={handleClick}>人员信息</button>
                <div className={`moveDetailsBox${flag ? 'Left' : 'Right'}`}>
                    <button className='Btn' onClick={handleClick}>收起</button>
                    <div className='userInfo'>
                        <div className='userInfoBox'>
                            <Ranking sortVotes={sortVote} />
                            {data.contestant && data.contestant.length > 0 ? (
                                data.contestant.map((item) => (
                                    <UserInfo data={item} />
                                ))
                            ) : (
                                <div>暂无人员信息</div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}