import React from 'react'
import './index.css'
import guan from '../img/champion.jpg'
import ya from '../img/runner-up.jpg'
import ji from '../img/runnerup.jpg'

export default function index({ sortVotes }) {
    let imgs = [guan, ya, ji]
    const Ranking = ({ className, id }) => (
        <div className={className}>
            <img className='img' src={imgs[id]} alt="" />
            <span className='names'>展无选手信息</span>
        </div>
    )
    return (
        <div className='ranking'>
            <div className='left'>
                {sortVotes.length > 1 ? (
                    <div className='top'>
                        <img className='img' src={imgs[1]} alt="" />
                        <span className='votes'>{sortVotes[1].votes}</span>
                        <span className='names'>{sortVotes[1].votes !== 0 ? sortVotes[1].name : '未分胜负'}</span>
                    </div>
                ) : (<Ranking className={'top'} id={1} />)}
                <div className='buttom'>
                </div>
            </div>
            <div className='middle'>
                {sortVotes.length > 0 ? (
                    <div className='top'>
                        <img className='img' src={imgs[0]} alt="" />
                        <span className='votes'>{sortVotes[0].votes}</span>
                        <span className='names'>{sortVotes[0].votes !== 0 ? sortVotes[0].name : '未分胜负'}</span>
                    </div>
                ) : (<Ranking className={'top'} id={0} />)}
                <div className='buttom'>
                </div>
            </div>
            <div className='right'>
                {sortVotes.length > 2 ? (
                    <div className='top'>
                        <img className='img' src={imgs[2]} alt="" />
                        <span className='votes'>{sortVotes[2].votes}</span>
                        <span className='names'>{sortVotes[2].votes !== 0 ? sortVotes[2].name : '未分胜负'}</span>
                    </div>
                ) : (
                    <Ranking className={'top'} id={2} />
                )}
                <div className='buttom'>
                </div>
            </div>
        </div>
    )
}
