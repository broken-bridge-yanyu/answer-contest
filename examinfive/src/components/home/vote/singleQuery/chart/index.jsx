import React, { useEffect, useRef } from 'react'
import * as echarts from 'echarts'

export default function Index({ echartsData }) {
    const chartRef = useRef(null);
    useEffect(() => {
        //初始化图表
        const myChart = echarts.init(chartRef.current)
        const option = {
            title: {
                text: echartsData.activityTitle,
                left: "center",
                textStyle: {
                    fontSize: 20,//标题字体大小
                    fontFamily: "serif",//字体
                    lineHeight: 50,//行高
                    textShadowColor: "rgba(0, 0, 0, 0.5)",//文字本身的阴影颜色。
                    textShadowBlur: 5,//文字本身的阴影长度。
                    textShadowOffsetX: 5,//文字本身的阴影 X 偏移。
                    textShadowOffsetY: 5//文字本身的阴影 Y 偏移。
                },
            },
            dataset: [
                //数据集1：定义为都和数据源
                {
                    dimensions: ['name', 'uuid', 'portait', '_id', 'votes'],
                    source: echartsData.contestant//数据源
                },
                //数据集2：对数据及逆行排序
                {
                    transform: {
                        type: 'sort',//排序得类型
                        config: { dimension: 'votes', order: 'desc' }//按照‘votes’字段进行降序排序
                    }
                }
            ],
            grid: {
                show: true,//是否显示网格
                top: "15%",//设置图标在容器中的上边距
                bottom: "15%"
            },
            xAxis: {
                type: 'category',//x轴类型
                axisLabel: {
                    interval: 0,
                    rotate: 30,
                    fontSize: 20,
                    margin: 20
                }//设置x轴刻度标签的显示间隔和旋转角度
            },
            yAxis: [{
                axisTick: {
                    show: true,//是否显示刻度线
                    alignWithLabel: true//刻度线和标签对其
                },
                minorTick: {
                    show: true,//是否显示次要的刻度线
                },
                axisLabel: {
                    show: true,//是否显示刻度标签
                    interval: 0,//设置标签的显示间隔
                    rotate: 50,//设置标签的旋转角度
                    fontWeight: "bold",//设置标签字体的加粗
                    fontSize: 15//设置标签字体大小
                }
            }],
            tooltip: {
                trigger: "axis",//触发类型为坐标轴触发
                axisPointer: {
                    type: "shadow"//坐标轴指示器类型为阴影
                }
            },
            series: [
                {
                    name: '票数',//系列名称
                    type: 'bar',//系列类型为柱状图
                    yAxisIndex: 0,//使用第一个y轴
                    encode: { x: 'name', y: 'votes' },//指定数据的维度映射关系，x轴映射到‘name’，y轴映射到‘votes’
                    datasetIndex: 1,//使用第二个数据集
                    colorBy: "data",//按照数据值进行颜色渲染
                    showBackground: true,//显示背景
                    label: {
                        show: true,//显示标签
                        fontWeight: "bold",//标签字体加粗
                        fontSize: 20,//标签字体大小
                    },
                    barWidth: 50
                },
            ]
        };
        myChart.setOption(option);//将配置项option应用于名为myChart的ECharts图标实例中。
        // 销毁图表
        return () => {
            myChart.dispose();
        };
    }, [echartsData.activityTitle, echartsData.contestant])

    return (
        <div className='singleQueryBoxButtomLeft'>
            <div ref={chartRef} style={{ width: '100%', height: '100%' }}>
            </div>
        </div>
    )
}
