//开始时间，结束时间，当前时间，进行判断过没过期。
function getActivityStatus(activityStartTime, activityEndTime) {//开始时间，结束时间，当前时间
    const startTime = new Date(activityStartTime); // 将活动开始时间字符串转换为 Date 对象
    const endTime = new Date(activityEndTime); // 将活动结束时间字符串转换为 Date 对象
    const currentTime = new Date()
    const given = new Date(currentTime); // 将给当前的时间字符串转换为 Date 对象
    if (given < startTime) {
        return 0;
    } else if (given > endTime) {
        return 2;
    } else {
        return 1;
    }
}
//时间格式转换
function formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    const hours = ('0' + date.getHours()).slice(-2);
    const minutes = ('0' + date.getMinutes()).slice(-2);
    return `${year}-${month}-${day} ${hours}:${minutes}`;
}

function convertTimestampToDate(timestamp) {
    return new Date(timestamp);
}


function timeNumber(listArray) {
    // 假设活动列表为 activities
    const currentTime = new Date()
    let overCount = 0;
    let conductCount = 0;
    let createCount = 0;
    listArray.forEach(item => {
        let activityOverTime = convertTimestampToDate(item.overTime)
        let activityCreateTime = convertTimestampToDate(item.createTime)
        if (activityOverTime < currentTime) {
            overCount++;
        } else if (activityCreateTime > currentTime) {
            createCount++;
        } else {
            conductCount++;
        }
    });
    return { overCount, createCount, conductCount }//结束的活动，未开始的活动，正在进行的活动
}
//深拷贝
function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
    if (Array.isArray(obj)) {
        const newArray = [];
        for (let i = 0; i < obj.length; i++) {
            newArray[i] = deepClone(obj[i]);
        }
        return newArray;
    }
    const newObj = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            newObj[key] = deepClone(obj[key]);
        }
    }
    return newObj;
}
export { getActivityStatus, formatDate, timeNumber, deepClone }
