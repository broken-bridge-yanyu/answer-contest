import React, { useState, useEffect } from 'react';
import QRcodeModule from './QRcodeModule'
import { ReloadOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import instance from '../../../../service/request'
import './QRCode.css'
export default function Index({ QRid }) {
  const navigate = useNavigate();
  const [isExpired, setIsExpired] = useState(false);
  const handleRefresh = () => {
    let id = QRid
    navigate(`/VotingPage?id=${id}`)
    setIsExpired(false);
    resetTimer();
  };
  const resetTimer = () => {
    clearTimeout(timerId);
    timerId = setTimeout(() => {
      setIsExpired(true);
    }, 1000 * 60 * 5);
  }
  let timerId;
  useEffect(() => {
    resetTimer();
    return () => clearTimeout(timerId);
  }, [timerId]);
  let value = 'https://www.cnblogs.com/dreambin/p/10078500.html'
  return (
    <div className='QRCodeBoxs'>
      {isExpired ? (
        <div>
          <p>投票渠道已关闭</p>
        </div>
      ) : (
        <div className='QRcodeBox'>
          <QRcodeModule value={value} />
          <QRcodeModule value={`https://www.caiyouyou.top/VotingPage?id=${QRid}`} />
          {/* <QRCode value={` https://eb72c80.r6.cpolar.top/VotingPage?id=${QRid}`} size={256} /> */}
          {/* <button onClick={handleRefresh}>Scan QR Code</button> */}
          <br />
        </div>
      )}
    </div>
  );
}



// http://ebb032c.r1.cpolar.top

