import React from 'react'
import QRCode from 'qrcode.react'
import ztLogo from '../../../../images/ztLogo.jpg'

export default function QRcodeModule({ value }) {
    return (
        <QRCode
            value={value}
            renderAs='svg'
            size={280}
            bgColor='#fff'
            fgColor='#0f2844'
            imageSettings={{
                src: ztLogo,  // 替换为你想要嵌入的图像路径
                height: 60,  // 图像的高度
                width: 60,   // 图像的宽度
                excavate: false  // 是否使用图像来代替黑色模块
            }}
        />
    )
}
