import React, { useState, useEffect, useCallback } from 'react'
import { debounce } from 'lodash'
import './index.css'
import { PostVoteApi } from '../../../../service/index'
import { Empty } from 'antd';
import { useNavigate, useSearchParams } from 'react-router-dom';
let baseURL = 'http://localhost:7321/'
export default function Index() {
    const navigate = useNavigate();
    let [dataList, setDataList] = useState([])
    const [searchParams] = useSearchParams();
    const id = searchParams.get("id")
    useEffect(() => {
        // dataLists()
        // 1、创建一个SSE连接
        const source = new EventSource(`${baseURL}serverSSE/returnVote/${id}`)
        // 2、监听SSE事件，open事件，message事件，ping事件，error事件
        source.addEventListener('open', (e) => {
            console.log(e + '服务器已连接');
        })
        //打开SSE信息
        source.addEventListener('message', (e) => {
            const data = e.data ? JSON.parse(e.data) : null
            setDataList(data)
        })
        //接收SSE自定义事件
        // source.addEventListener('ping', (e) => {
        //     console.log('接收SSE自定义事件信息', e);
        // })
        source.addEventListener('error', (e) => {
            // @ts-ignore
            if (e.currentTarget.readyState === 0) {
                console.log('SSE连接已关闭');
            }
        })
        // 在窗口即将关闭时关闭SSE连接
        window.addEventListener('beforeunload', () => {
            source.close();
        });
        return () => {
            // 组件卸载时关闭SSE连接
            source.close();
            console.log('已经于SSE连接关闭');
        };
    }, [id])

    // const actionVotes = useCallback(debounce((id) => {//防抖
    //     PostVoteApi(`vote/voteAction/${dataList[0]._id}`, { id, dataList }).then(res => {
    //         let { code, msg } = res
    //         if (code === 200) navigate(`/SucceedPage?id=${msg}`)
    //     })
    // }, 1000), [dataList, navigate])


    // 创建一个队列用于存放待发送的请求
    const requestQueue = [];
    let isProcessing = false; // 标记是否正在处理请求
    const processQueue = () => {
        if (!isProcessing && requestQueue.length > 0) {
            isProcessing = true;
            const { id, dataList } = requestQueue.shift(); // 取出队列中的请求
            PostVoteApi(`vote/voteAction/${dataList[0]._id}`, { id, dataList })
                .then(res => {
                    let { code, msg } = res;
                    if (code === 200) navigate(`/SucceedPage?id=${msg}`);
                })
                .finally(() => {
                    isProcessing = false;
                    processQueue(); // 处理完一个请求后继续处理队列中的下一个请求
                });
        }
    };

    const MAX_QUEUE_LENGTH = 100; // 最大队列长度
    const actionVotes = useCallback(
        debounce((id) => {
            if (requestQueue.length < MAX_QUEUE_LENGTH) {
                // 将新的请求添加到队列中
                requestQueue.push({ id, dataList });
                processQueue(); // 尝试处理队列中的请求
            } else {
                console.log('队列已满，拒绝新请求');
                // 这里可以添加相关的处理逻辑，例如丢弃最早的请求或者给予用户提示
            }
        }, 1000),
        [dataList, navigate]
    );


    return (
        <div className='votingPage'>
            <div className='top'>
                <span className='title'>{dataList[0]?.activityTitle}</span>
                {/* <img className='cover' src={`http://154.8.152.188:7321/${dataList.activityCover}`} alt="" /> */}
                <img className='cover' src={`http://localhost:7321/${dataList[0]?.activityCover}`} alt="" />
                <div className='explain'>
                    <span className='notice'>
                        <span className='noticeTitle'>注意事项：</span>
                        <span className='noticeContent'>{dataList[0]?.describe}</span>
                    </span>
                </div>
            </div>
            <div className='buttom'>
                {dataList[0] === undefined ? (
                    <Empty />
                ) : (
                    dataList[0].contestant.map(item => (
                        <div className='contestantBox' key={item._id}>
                            {/* <img className='imgHand' src={`http://154.8.152.188:7321/${item.portrait}`} alt="" /> */}
                            <img className='imgHand' src={`http://localhost:7321/${item.portrait}`} alt="" />
                            <div className='personInfo'>
                                <span className='name'>{item.name}</span>
                                <span className='votes'>票数：{item.votes}</span>
                                <button className='voteBtn' onClick={() => actionVotes(item)}>投票</button>
                            </div>
                        </div>
                    ))
                )}
            </div>
        </div>
    )
}
