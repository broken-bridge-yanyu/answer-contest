import React from 'react'
import { SmileOutlined } from '@ant-design/icons';
import { Result } from 'antd';
import { useSearchParams } from 'react-router-dom'

export default function SucceedPage() {
    const [searchParams] = useSearchParams();
    const msg = searchParams.get("id")
    return (
        <div style={{ height: '100%' }}>
            <Result
                style={{ height: '100%' }}
                icon={<SmileOutlined />}
                title={msg}
            />
        </div>

    )
}
