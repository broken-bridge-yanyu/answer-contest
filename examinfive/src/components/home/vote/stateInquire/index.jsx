import React, { useEffect, useState } from 'react'
import './index.css'
import { getActivityStatus, formatDate } from '../timeModel'
import { Tag, Empty, message, Modal, Popconfirm } from 'antd';
import ReturnTopBtn from '../../../../function/functionBox/vote/returnTopBtn/returnTopBtn.jsx'
import { GetVoteApi, DeleteVoteApi } from '../../../../service/index'
import QR from '../QRcode/QRcode.jsx'
import { useSearchParams, useNavigate } from 'react-router-dom'

export default function Index() {
    const navigate = useNavigate();
    const [messageApi, contextHolder] = message.useMessage();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [ID, setID] = useState(false);
    const showModal = (id) => {
        setID(id._id)
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };
    const [searchParams] = useSearchParams();
    const id = Number(searchParams.get("id"))
    let [newData, setNewData] = useState([])
    useEffect(() => {
        data()
    }, [id])
    const data = () => {
        GetVoteApi('vote/listParticipant').then(res => {
            let { code, data } = res
            if (code === 200) {
                let newDatas = []
                data.forEach(item => {
                    let ids = getActivityStatus(item.createTime, item.overTime)
                    if (ids === id) newDatas.push(item)
                });
                // @ts-ignore
                setNewData(newDatas)
            }
        })
    }
    const delOne = (id) => {
        DeleteVoteApi(`vote/delParticipant/${id._id}`).then(res => {
            let { code, msg } = res
            if (code === 200) {
                messageApi.open({
                    type: 'success',
                    content: msg,
                });
                data()
            }
        })
    }
    const votesResult = (id) => {
        navigate(`/VotingInterface?id=${id}`)
    }
    const cancel = () => {
        message.error('您取消了当前操作');
    };
    return (
        <div className='examineBox'>
            {contextHolder}
            <ReturnTopBtn pathNavigate={'/home/vote'} />
            <div className='examineBoxBody'>
                {newData.length === 0 ? (
                    <Empty />
                ) : (
                    newData.map(item => (
                        <div key={item._id} className='examineBox-box' >
                            <div className='examineBox-boxLeft'>
                                {/* <img src={`http://154.8.152.188:7321/${item.activityCover}`} alt="" /> */}
                                <img src={`http://localhost:7321/${item.activityCover}`} alt="" />
                            </div>
                            <div className='examineBox-boxCentre'>
                                <span className='titleBox'>
                                    <span className='state'>
                                        <Tag
                                            className='ant-tag-state'
                                            style={getActivityStatus(item.createTime, item.overTime,) === 0 ? {} : { display: 'none' }}
                                            color="#fd7921"
                                        >未开始</Tag>
                                        <Tag
                                            className='ant-tag-state'
                                            style={getActivityStatus(item.createTime, item.overTime,) === 1 ? {} : { display: 'none' }}
                                            color="#87d068"
                                        >进行中</Tag>
                                        <Tag
                                            className='ant-tag-state'
                                            style={getActivityStatus(item.createTime, item.overTime,) === 2 ? {} : { display: 'none' }}
                                            color="#b3b4b8"
                                        >已结束</Tag>
                                    </span>
                                    <span className='title'>{item.activityTitle}</span>
                                </span>
                                <span className='createTime'>创建时间：{formatDate(item.establishTime)}</span>
                                <span className='startTime'>开始时间：{formatDate(item.createTime)}</span>
                                <span className='overTime'>结束时间：{formatDate(item.overTime)}</span>
                                <span className='funBox'>
                                    <div className='team'>
                                        <span >选手：<span >{item.contestant.length || 0}</span></span>
                                    </div>
                                    <div className='vote'>
                                        <span >票数：<span >{item.contestant.reduce((total, item) => total + item.votes, 0)}</span></span>
                                    </div>
                                </span>
                            </div>
                            <div className='examineBox-boxRight'>
                                <div className='QRcode'>
                                    <button
                                        className={getActivityStatus(item.createTime, item.overTime) !== 1 ? 'examineBox-boxRight-ButtonQRcodeNot-allowed' : 'examineBox-boxRight-ButtonQRcodePointer'}
                                        disabled={getActivityStatus(item.createTime, item.overTime) !== 1}
                                        onClick={() => showModal(item)}
                                    >
                                        {getActivityStatus(item.createTime, item.overTime) === 0 ? '活动还未开始' : ''}
                                        {getActivityStatus(item.createTime, item.overTime) === 1 ? '二维码投票' : ''}
                                        {getActivityStatus(item.createTime, item.overTime) === 2 ? '投票结束' : ''}
                                    </button>
                                    <Modal title="投票二维码" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width={320}>
                                        <QR QRid={ID}></QR>
                                    </Modal>
                                </div>
                                <div className='upd'>
                                    <button disabled={getActivityStatus(item.createTime, item.overTime) !== 1 && getActivityStatus(item.createTime, item.overTime) !== 2} className='examineBox-boxRight-ButtonUpd' onClick={() => votesResult(item._id)}>投票结果</button>
                                </div>
                                <div className='del'>
                                    <Popconfirm
                                        title="删除任务"
                                        description="您确定要删除此任务吗?"
                                        okText="是"
                                        cancelText="否"
                                        onConfirm={() => {
                                            delOne(item)
                                        }}
                                        onCancel={cancel}
                                    >
                                        <button className='examineBox-boxRight-ButtonDel' >删除</button>
                                    </Popconfirm>
                                </div>
                            </div>
                        </div>
                    ))
                )}
            </div>
        </div >
    )
}
