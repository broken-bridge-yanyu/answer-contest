import React, { useEffect, useState } from 'react'
import { Input, Button, DatePicker, Switch, Select, Drawer, Space, message, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import './index.css'
import { v4 as uuidv4 } from 'uuid'
import { PutVoteApi, GetVoteApi } from '../../../../service/index'
import { imgUrl } from '../../../../service/request'
import { fn } from '../../../message/index'
import { formatDate, deepClone } from '../timeModel'
import dayjs from 'dayjs'
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;

export default function Index({ Updata, IsModalOpen, HandleOk, HandleCancel, UpdataVal, CreateArray, DelArray }) {
    useEffect(() => {
        stuData()
    }, [Updata])
    let [state, setState] = useState(Updata.channelState)
    const activeState = (value) => {
        PutVoteApi(`vote/activeStateUpdata/${Updata._id}/${value}`).then(res => {
            let { code, msg, state } = res
            setState(state)
            fn(code, value ? msg + "开启" : msg + "关闭")
        })
    }
    //学生选手列表
    let [stuDatas, setStuData] = useState([])
    const stuData = async () => {
        try {
            const res = await GetVoteApi('personnel/list');
            const processedData = res.data.map(item => ({
                value: item._id, label: item.username
            }));
            setStuData(processedData);
        } catch (error) {
            console.error('获取数据出错：', error);
        }
    };
    const change = (val, index) => {
        let { name, value, ind } = val.target
        if (name === 'stu') {
            const newData = [...Updata.contestant]
            newData[index].personnel = value
            UpdataVal(deepClone(Updata));
        } else if (name === 'name' || name === 'slogan') {
            let newName = [...Updata.contestant]
            newName[index][name] = value
            UpdataVal(deepClone(Updata));
        } else if (name === 'activityCover') UpdataVal({ ...Updata, [name]: value });
        else if (name === 'portrait') {
            let newName = [...Updata.contestant]
            newName[ind][name] = value
        } else if (name === 'time') {
            UpdataVal({
                ...Updata,
                createTime: value && value.length > 0 ? formatDate(value[0]) : null,
                overTime: value && value.length > 0 ? formatDate(value[1]) : null,
            });
        } else {
            UpdataVal({ ...Updata, [name]: value })
        };
    }
    const addArray = () => CreateArray({ name: '', slogan: '', uuid: uuidv4(), img: [], personnel: [] })
    //删除数组中其中一列
    //找到要删除的数据 调用父组件提供的 DelArray 函数，传递更新后的数据
    const delAry = (uuid) => DelArray(Updata.contestant.filter(item => item.uuid !== uuid));
    //图片上传
    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) message.error('You can only upload JPG/PNG file!');
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) message.error('Image must smaller than 2MB!');
        return isJpgOrPng && isLt2M;
    };
    let userId = JSON.parse(localStorage.getItem('userId'))
    const [loading, setLoading] = useState(false);
    const handleChange = (info) => {
        if (info.file.status === 'uploading') setLoading(!loading);
        if (info.file.response) {
            change({ target: { name: 'activityCover', value: info.file.response.path } })
            setLoading(!loading);
        }
    };
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );
    //小组/成员图片的修改
    const beforeUploadStaff = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) message.error('You can only upload JPG/PNG file!');
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) message.error('Image must smaller than 2MB!');
        return isJpgOrPng && isLt2M;
    };
    const [loadingStaff, setLoadingStaff] = useState(false);
    const handleChangeStaff = (index) => (info) => {
        if (info.file.status === 'uploading') setLoadingStaff(!loadingStaff);
        if (info.file.response) {
            change({ target: { name: 'portrait', value: info.file.response.path, ind: index } })
            setLoadingStaff(!loadingStaff);
        }
    };
    const uploadButtonStaff = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loadingStaff ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );
    return (
        <Drawer title="Basic Modal"
            open={IsModalOpen}
            onClose={HandleCancel}
            placement={'top'}
            height={'100%'}
            extra={
                <Space>
                    <Button onClick={HandleCancel}>Cancel</Button>
                    <Button type="primary" onClick={HandleOk}>
                        OK
                    </Button>
                </Space>
            }
        >
            <div className='activityBox'>
                <div className='activityMessage'>
                    <div className='img'>
                        <Upload
                            name="avatar"
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            action={`${imgUrl}vote/imageUpload/${userId}`}
                            beforeUpload={beforeUpload}
                            onChange={handleChange}
                        >
                            {Updata.activityCover ? (
                                <img
                                    className='imgActivity'
                                    src={imgUrl + Updata.activityCover}
                                    alt="avatar"
                                />
                            ) : (
                                uploadButton
                            )}
                        </Upload>
                    </div>
                    <div className='message'>
                        <label className="labelBox">
                            <span className='labelHead'>活动名称：</span>
                            <Input className='ant-inputs' value={Updata.activityTitle} name='activityTitle' onChange={e => change(e)} />
                        </label>
                        <label className="labelBox">
                            <span className='labelHead'>活动时间：</span>
                            <RangePicker
                                showTime={{
                                    format: 'HH:mm',
                                }}
                                format="YYYY-MM-DD HH:mm"
                                defaultValue={[dayjs(Updata.createTime, 'YYYY-MM-DD HH:mm'), dayjs(Updata.overTime, 'YYYY-MM-DD HH:mm')]}
                                onChange={value => change({ target: { name: 'time', value } })}
                            />
                        </label>
                        <label className="labelBox">
                            <span className='labelHead'>活动描述：</span>
                            <TextArea name='describe' showCount maxLength={100} value={Updata.describe} onChange={e => change(e)} placeholder="can resize" />
                        </label>
                        <label className="labelBox">
                            <span className='labelHead'>活动状态：</span>
                            <Switch value={state} onChange={value => activeState(value)} checkedChildren="开启" unCheckedChildren="关闭" defaultChecked />
                        </label>
                    </div>
                </div>
                <div className='activityMember'>
                    <Button onClick={addArray} type="primary" ghost>
                        添加一行
                    </Button>
                    <div className='memberBox'>
                        {
                            Updata?.contestant?.map((item, index) => (
                                <div className='memberbox' key={item._id}>
                                    <Upload
                                        name="avatar"
                                        listType="picture-card"
                                        className="avatar-uploader"
                                        showUploadList={false}
                                        action={`${imgUrl}vote/imageUpload/${userId}`}
                                        beforeUpload={beforeUploadStaff}
                                        onChange={handleChangeStaff(index)}
                                    >
                                        {item.portrait ? (
                                            <img
                                                className='imgStaff'
                                                src={imgUrl + item.portrait}
                                                alt="avatar"
                                                style={{
                                                    width: '100%',
                                                }}
                                            />
                                        ) : (
                                            uploadButtonStaff
                                        )}
                                    </Upload>
                                    <div className='memberDiv'>
                                        <Input value={item.name} name='name' onChange={e => change(e, index)} />
                                        <Input value={item.slogan} name='slogan' onChange={e => change(e, index)} />
                                        <Select //选择器，根据名字进行搜索
                                            mode="multiple"
                                            style={{
                                                width: '100%',
                                            }}
                                            placeholder="请选择参赛人员"
                                            // onChange={e => change(e, index)}
                                            onChange={value => change({ target: { name: 'stu', value } }, index)}
                                            options={stuDatas}
                                            showSearch // 启用搜索功能
                                            filterOption={(input, option) =>
                                                option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            } // 根据选项标签进行过滤
                                            value={Updata?.contestant[index].personnel}
                                        >
                                            {stuDatas.map((item) => (
                                                <Option key={item.value} value={item.value}>
                                                    {item.label}
                                                </Option>
                                            ))}
                                        </Select>
                                    </div>
                                    <Button type="primary" onClick={() => delAry(item.uuid)} shape="circle" icon={<DeleteOutlined />} />
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </Drawer >
    )
}
