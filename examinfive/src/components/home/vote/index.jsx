import React, { useEffect, useState } from 'react'
import './index.css'
import { Button, Tooltip, Typography, Empty, Table, Tag, message, Popconfirm } from 'antd';
import { EyeOutlined, PlusSquareOutlined, FormOutlined, DeleteOutlined } from '@ant-design/icons';
import { getActivityStatus, formatDate } from './timeModel'
import { GetVoteApi, DeleteVoteApi, PostVoteApi } from '../../../service/index'
import { useNavigate } from 'react-router-dom';
import ButtomModules from './buttomModule'
import UpdateLatextActivity from './updateLatextActivity'
const { Title } = Typography;
export default function Index() {
  const [messageApi, contextHolder] = message.useMessage();
  const navigate = useNavigate();
  useEffect(() => {
    listParticipant()
  }, []);
  //活动列表
  let [latestActivity, setLatestActivity] = useState([])
  const listParticipant = async () => {
    GetVoteApi('vote/listParticipant').then(res => {
      // @ts-ignore
      let { code, data } = res
      if (code === 200) setLatestActivity(data)
    })
  }
  //表格操作
  const tableRowDel = async (id) => {//删除
    DeleteVoteApi(`vote/delParticipant/${id._id}`).then(res => {
      let { code, msg } = res
      if (code === 200) {
        messageApi.open({
          type: 'success',
          content: msg,
        });
        listParticipant()
      }
    })
  }
  const activityOne = (id) => navigate(`/SingleQuery?id=${id}`)
  const cancel = () => {
    message.error('您取消了当前操作');
  };
  const columns = [
    {
      title: '活动名称',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      width: '200px'
    },
    {
      title: '参赛小组/选手数量',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      align: 'center',
      render: (_, record) => (
        <span>{record.contestant.length}</span>
      ),
      width: '150px'
    },
    {
      title: '总票数',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      align: 'center',
      width: '80px',
      render: (_, record) => (
        <span>{record.contestant.reduce((total, item) => total + item.votes, 0)}</span>
      ),
    },
    {
      title: '活动状态',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      render: (_, record) => (
        <div key={record._id}>
          <Tag style={getActivityStatus(record.createTime, record.overTime) === 0 ? {} : { display: 'none' }} color="#b3b4b8">未开始</Tag>
          <Tag style={getActivityStatus(record.createTime, record.overTime) === 1 ? { fontSize: '14px' } : { display: 'none' }} color="#87d068">正在进行中</Tag>
          <Tag style={getActivityStatus(record.createTime, record.overTime) === 2 ? {} : { display: 'none' }} color="#fd7921">已经结束</Tag>
        </div>
      )
    },
    {
      title: '活动时间',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      align: 'center',
      render: (_, record) => (
        <div key={record._id} style={{ fontSize: '12px' }}>
          <p>开始：{formatDate(record.createTime)}</p>
          <p>结束：{formatDate(record.overTime)}</p>
          <p>创建：{formatDate(record.establishTime)}</p>
        </div>
      )
    },
    {
      title: '操作',
      dataIndex: 'activityTitle',
      key: 'activityTitle',
      align: 'center',
      render: (_, record) => (
        <div>
          <Button type="primary" shape="circle" onClick={() => activityOne(record._id)} icon={<EyeOutlined />} />
          <Button type="primary" shape="circle" onClick={() => activityUpdate(record)} icon={<FormOutlined />} />
          <Popconfirm
            title="删除任务"
            description="您确定要删除此任务吗?"
            okText="是"
            cancelText="否"
            onConfirm={() => {
              tableRowDel(record)
            }}
            onCancel={cancel}
          >
            <Button type="primary" shape="circle" icon={< DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ]
  const jump = () => navigate('/home/addLatestActivity')
  //底部状态
  const UnplayedBoxLook = () => (
    <div>
      <span className='lookList'>查看列表</span>
      <span className='buttomModule icon-youjiantou'></span>
    </div>
  )
  const stateInquire = (state) => navigate(`/home/StateInquire?id=${state}`)


  //活动修改
  const [isModalOpen, setIsModalOpen] = useState(false);
  let [updata, setUpdata] = useState({})
  const activityUpdate = (data) => {
    setUpdata(data)
    setIsModalOpen(!isModalOpen);
  }
  const changeVal = (data) => setUpdata(data)

  const createArray = (data) => {//添加参赛人员/小组
    setUpdata((ary) => ({
      ...ary,
      contestant: [...ary.contestant, data]
    }))
  }
  const delArray = (data) => {//添加参赛人员/小组
    setUpdata(delData => ({
      ...delData,
      contestant: data
    }))
  }
  const handleOkUpdata = () => {
    PostVoteApi(`vote/activeUpdata/${updata._id}`, updata).then(res => {
      let { code, msg } = res
      if (code === 200) {
        listParticipant()
        setUpdata({})
      }
    })
    setIsModalOpen(false);
  };
  const handleCancel = () => setIsModalOpen(false);
  return (
    <div className='Participants'>
      {/* 活动列表 */}
      {contextHolder}
      <div className='latestActivity'>
        <div className='latestActivity-top'>
          <div className='latestActivity-top-left'>
            <Title level={2}>活动列表</Title>
          </div>
          <div className='latestActivity-top-right'>
            <div className='buttonBox'>
              <Tooltip title="新建活动">
                <Button type="primary" onClick={jump} ghost icon={< PlusSquareOutlined />} ></Button>
              </Tooltip>
            </div>
          </div>
        </div>
        <div className={latestActivity.length === 0 ? 'latestActivity-buttoms' : 'latestActivity-buttom'}>
          <Empty
            image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
            imageStyle={{
              height: 60,
            }}
            style={latestActivity.length !== 0 ? { display: 'none' } : {}}
            description={
              <span>
                空空如也！
              </span>
            }
          >
            <Button type="primary" onClick={jump}>立即创建</Button>
          </Empty>
          <div className='latestActivity-buttomBodyBox' style={latestActivity.length === 0 ? { display: 'none' } : {}}>
            <Table
              scroll={{
                x: 500,
                y: 310,
              }}
              virtual
              columns={columns}
              dataSource={latestActivity}
              pagination={false}
              rowKey={'_id'}
            />
          </div>
        </div>
        {/* 底部任务查看状态 */}
        <ButtomModules latestActivityData={latestActivity} UnplayedBoxLook={UnplayedBoxLook} handlOneLook={stateInquire} />
        {/* 数据单个修改 */}
        <UpdateLatextActivity Updata={updata} IsModalOpen={isModalOpen} HandleOk={handleOkUpdata} HandleCancel={handleCancel} UpdataVal={changeVal} CreateArray={createArray} DelArray={delArray} />
      </div>
    </div>
  )
}
