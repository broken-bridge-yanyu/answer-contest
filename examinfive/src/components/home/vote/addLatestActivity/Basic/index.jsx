import React, { useState } from 'react'
import { Input, DatePicker, Space, Upload, message } from 'antd';
import './index.css'
import useBeforeUnload from '../refresh'
import { PlusOutlined, LoadingOutlined } from '@ant-design/icons';
import { imgUrl } from '../../../../../service/request'
import { formatDate } from '../../timeModel'
const { RangePicker } = DatePicker;
const { TextArea } = Input;
export default function Basic({ basicData, ChangeVal }) {
    let userId = JSON.parse(localStorage.getItem('userId'))
    useBeforeUnload()
    //添加
    const change = (val) => {
        let { name, value } = val.target
        if (name === 'activityCover') ChangeVal({ ...basicData, [name]: value })
        else if (name === 'time') {
            ChangeVal({
                ...basicData,
                createTime: value && value.length > 0 ? formatDate(value[0]) : null,
                overTime: value && value.length > 0 ? formatDate(value[1]) : null,
            });
        } else ChangeVal({ ...basicData, [name]: value });
    }
    //图片上传活动封面上传
    const [loading, setLoading] = useState(false);
    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) message.error('You can only upload JPG/PNG file!');
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) message.error('Image must smaller than 2MB!');
        return isJpgOrPng && isLt2M;
    };
    const handleChange = (info) => {
        if (info.file.status === 'uploading') setLoading(!loading);
        if (info.file.response) {
            change({ target: { name: 'activityCover', value: info.file.response.path } })
            setLoading(!loading);
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );
    return (
        <div className='content'>
            <div className='contentBody'>
                <div className='contentBodyTop'>
                    <h1>活动基本信息</h1>
                </div>
                <div className='contentBodyButtoms'>
                    <div className='contentBodyButtom_Body'>
                        <div className='inputBox'>
                            <label className="inputTitle">活动名称：</label>
                            <Input placeholder="请输入活动名称" value={basicData.activityTitle} onChange={change} name='activityTitle' />
                        </div>
                        <div className='timeBox'>
                            <label className="timeTitle">活动名称：</label>
                            <Space direction="vertical" size={12}>
                                <RangePicker
                                    showTime={{
                                        format: 'HH:mm',
                                    }}
                                    format="YYYY-MM-DD HH:mm"
                                    onChange={value => change({ target: { name: 'time', value } })}
                                />
                            </Space>
                        </div>
                        <div className='textBox'>
                            <label className="textTitle">活动描述：</label>
                            <TextArea
                                showCount
                                maxLength={100}
                                value={basicData.describe}
                                name='describe'
                                onChange={e => change(e)}
                                placeholder="描述"
                                style={{
                                    height: 120,
                                    resize: 'none',
                                }}
                            />
                        </div>
                        <div className='imgs'>
                            <label className="imgsTitle">
                                <span className='labelTitle'>活动图片：</span>
                                <Upload
                                    name="avatar"
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    showUploadList={false}
                                    action={`${imgUrl}vote/imageUpload/${userId}`}
                                    beforeUpload={beforeUpload}
                                    onChange={handleChange}
                                >
                                    {basicData.activityCover ? (
                                        <img
                                            className='imgActivity'
                                            src={imgUrl + basicData.activityCover}
                                            alt="avatar"
                                        />
                                    ) : (
                                        uploadButton
                                    )}
                                </Upload>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
