import { useEffect } from 'react'

function useBeforeUnload() {
    useEffect(() => {
        // @ts-ignore

        const handleBeforeUnload = (event) => {
            event.preventDefault();
            event.returnValue = '确定要离开吗？';
        };
        window.addEventListener('beforeunload', handleBeforeUnload);
        return () => {
            // 组件卸载时，取消事件监听
            window.removeEventListener('beforeunload', handleBeforeUnload);
        };
    }, []); // 空数组表示只在组件挂载和卸载时执行一次
}

export default useBeforeUnload;