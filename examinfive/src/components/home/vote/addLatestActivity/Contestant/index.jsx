import React, { useState, useEffect } from 'react'
import './index.css'
import { PlusOutlined, DeleteOutlined, LoadingOutlined } from '@ant-design/icons';
import { Upload, Button, Input, Select, message, Popconfirm } from 'antd';
import { imgUrl } from '../../../../../service/request'
import { v4 as uuidv4 } from 'uuid'
import useBeforeUnload from '../refresh'
import { GetVoteApi } from '../../../../../service/index'
import { deepClone } from '../../timeModel'
const { Option } = Select;

export default function Contestant({ activityData, contestantAddArrays, contestantDelObjs, ChangeVal }) {
    // @ts-ignore
    let userId = JSON.parse(localStorage.getItem('userId'))
    useBeforeUnload()
    //添加一个小组/选手
    const handleAddUser = () => contestantAddArrays({ name: '', slogan: '', id: uuidv4(), img: [], personnel: [] })
    //删除一个小组/选手
    const handleRemoveUser = (uuid) => {
        contestantDelObjs(uuid)
    }
    //选手列表
    useEffect(() => {
        stuData()
    }, [])
    let [stuDatas, setStuData] = useState([])
    const stuData = async () => {
        try {
            const res = await GetVoteApi('personnel/list');
            const processedData = res.data.map(item => ({
                value: item._id, label: item.username
            }));
            setStuData(processedData);
        } catch (error) {
            console.error('获取数据出错：', error);
        }
    };
    //添加
    const change = (val, index) => {
        let { name, value, ind } = val.target
        if (name === 'portrait') { //图片
            let newName = [...activityData.conteStant]
            newName[ind][name] = value
            ChangeVal(deepClone(activityData))
        } else if (name === 'name' || name === 'slogan') { //名称//宣言
            let newName = [...activityData.conteStant]
            newName[index][name] = value
            ChangeVal(deepClone(activityData))
        } else { // 小组人员
            const newData = [...activityData.conteStant]
            newData[index].personnel = value
            ChangeVal(deepClone(activityData))
        }
    }
    //图片上传
    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) message.error('You can only upload JPG/PNG file!');
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) message.error('Image must smaller than 2MB!');
        return isJpgOrPng && isLt2M;
    };
    const [loading, setLoading] = useState(false);
    const handleChange = (index) => (info) => {
        if (info.file.status === 'uploading') setLoading(!loading);
        if (info.file.response) {
            change({ target: { name: 'portrait', value: info.file.response.path, ind: index } })
            setLoading(!loading);
        }
    };
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </button>
    );
    const cancel = () => {
        message.error('您取消了当前操作');
    };
    return (
        <div className='content'>
            <div className='contentBody'>
                <div className='contentBodyTop'>
                    <h1>参赛选手/小组基本信息</h1>
                </div>
                <div className='contentBodyButtom'>
                    <Button type="primary" onClick={handleAddUser} ghost>添加一行</Button>
                    <div className='contentBodyButtom_Body'>
                        {
                            activityData.conteStant.map((item, index) => (
                                <div className={activityData.conteStant.length === 1 ? 'participantBox' : 'participantBoxs'} key={index}>
                                    <div className='photo'>
                                        <Upload
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            showUploadList={false}
                                            action={`${imgUrl}vote/imageUpload/${userId}`}
                                            beforeUpload={beforeUpload}
                                            onChange={handleChange(index)}
                                        >
                                            {item.portrait ? (
                                                <img
                                                    className='imgStaff'
                                                    src={imgUrl + item.portrait}
                                                    alt="avatar"
                                                    style={{
                                                        width: '100%',
                                                    }}
                                                />
                                            ) : (
                                                uploadButton
                                            )}
                                        </Upload>
                                    </div>
                                    <div className='inputBox'>
                                        <Input type="text" name='name' value={item.name} placeholder='请输入名称' onChange={e => change(e, index)} />
                                        <Input type="text" name='slogan' value={item.slogan} onChange={e => change(e, index)} placeholder='誓言' />
                                        <Select //选择器，根据名字进行搜索
                                            mode="multiple"
                                            style={{
                                                width: '100%',
                                            }}
                                            placeholder="请选择参赛人员"
                                            onChange={value => change({ target: { name: 'stu', value } }, index)}
                                            options={stuDatas}
                                            showSearch // 启用搜索功能
                                            filterOption={(input, option) =>
                                                option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            } // 根据选项标签进行过滤
                                        >
                                            {stuDatas.map((item) => (
                                                <Option key={item.value} value={item.value}>
                                                    {item.label}
                                                </Option>
                                            ))}
                                        </Select>
                                    </div>
                                    <div className='btnBox'>
                                        <Popconfirm
                                            title="删除任务"
                                            description="您确定要删除此任务吗?"
                                            okText="是"
                                            cancelText="否"
                                            onConfirm={() => {
                                                handleRemoveUser(item.id)
                                            }}
                                            onCancel={cancel}
                                        >
                                            <Button
                                                style={activityData.conteStant.length === 1 ? { display: 'none' } : {}}
                                                type="primary" shape="circle" icon={<DeleteOutlined />}>
                                            </Button >
                                        </Popconfirm>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}
