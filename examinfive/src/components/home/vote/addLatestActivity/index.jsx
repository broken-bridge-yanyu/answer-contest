import React, { useState } from 'react';
import './index.css'
import { Button, Steps, theme, message } from 'antd';
import Basic from './Basic'//基本信息
import Contestant from './Contestant'//参赛选手/小组基本信息
import useBeforeUnload from './refresh'
import { v4 as uuidv4 } from 'uuid'
import { useNavigate } from 'react-router-dom';
import ReturnTopBtn from '../../../../function/functionBox/vote/returnTopBtn/returnTopBtn'
import { PostVoteApi } from '../../../../service/index'
import { fn } from '../../../message/index'


export default function Index() {
    const [messageApi, contextHolder] = message.useMessage();
    const navigate = useNavigate();
    useBeforeUnload()
    const { token } = theme.useToken();
    const [current, setCurrent] = useState(0);
    //信息
    const [activity, setActivity] = useState({
        //基本信息
        activityCover: '',//活动封面
        activityTitle: '默认标题（标题可自定义）',//标题
        createTime: '',//开始时间
        overTime: '',//结束时间
        describe: '',//活动描述
        //选手信息
        conteStant: [
            {
                id: uuidv4(),
                name: '',
                slogan: '',
                personnel: [],
                portrait: '',
            }
        ]
        //样式
    })

    const contestantAddArray = (newData) => {//添加添加一列
        setActivity((prevActivity) => ({
            ...prevActivity,
            conteStant: [...prevActivity.conteStant, newData],
        }));
    }
    const changeVal = (data) => setActivity(data)
    const contestantDelObj = (uuid) => {//删除删除小组/成员其中一列
        // @ts-ignore
        setActivity(() => {
            const newActivityConteStant = activity.conteStant.filter(
                (contestant) => contestant?.id !== uuid // 使用 id 属性判断选手对象是否需要被删除
            );
            return {
                ...activity,
                conteStant: newActivityConteStant,
            };
        });
        activity.conteStant.forEach(async (item) => {//删除一个选手信息时，同时也删除对应的图片
            if (item.id === uuid) PostVoteApi('vote/imageDelete', { path: item.portrait })
        })
    }
    const next = () => {
        const { activityCover, createTime, overTime } = activity;
        // 检查字段是否为空
        if (activityCover === []) {
            return messageApi.open({
                type: 'warning',
                content: '请添加一张图片！',
            });
        } else if (createTime === '' && overTime === '') {
            return messageApi.open({
                type: 'warning',
                content: '请填将活动时间填写一下！',
            });
        } else setCurrent(current + 1);
    };
    const prev = () => setCurrent(current - 1);

    const steps = [
        {
            title: '活动基本信息',
            content: <Basic basicData={activity} ChangeVal={changeVal} />,
        },
        {
            title: '参赛选手/小组基本信息',
            content: <Contestant activityData={activity} contestantAddArrays={contestantAddArray} contestantDelObjs={contestantDelObj} ChangeVal={changeVal} />,
        },
    ];
    const items = steps.map((item) => ({
        key: item.title,
        title: item.title,
    }));
    const contentStyle = {//样式
        color: token.colorTextTertiary,
        backgroundColor: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        border: `1px dashed ${token.colorBorder}`,
        marginTop: 16,
    };
    //提交
    const submitBtn = async () => {
        PostVoteApi('vote/addParticipant', activity).then(res => {
            let { code, msg } = res
            if (code === 200) navigate('/home/vote')
            fn(code, msg)
        })
    }
    return (
        <div className='addLatestActivity'>
            {contextHolder}
            <ReturnTopBtn pathNavigate={'/home/vote'} />
            <Steps current={current} items={items} />
            <div className='contentArea' style={contentStyle}>{steps[current].content}</div>
            <div className='bottom'
                style={{
                    marginTop: 24,
                }}
            >
                {current < steps.length - 1 && (
                    <Button type="primary" onClick={() => next()}>
                        下一项
                    </Button>
                )}
                {current === steps.length - 1 && (
                    <Button type="primary" onClick={() => submitBtn()} >
                        提交
                    </Button>
                )}
                {current > 0 && (
                    <Button
                        style={{
                            margin: '0 8px',
                        }}
                        onClick={() => prev()}
                    >
                        上一项
                    </Button>
                )}
            </div>
        </div>
    )
}
