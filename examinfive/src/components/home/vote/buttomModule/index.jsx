import React from 'react'
import { timeNumber } from '../timeModel'
import './index.css'
import '../../../../function/icon/vote/voteIcon/iconfont.css'
import '../../../../function/icon/vote/rightJt/iconfont.css'

export default function Index({ latestActivityData, UnplayedBoxLook, handlOneLook }) {
    return (
        <div className='f4'>
            <div className='unplayed'>
                <div className='unplayedBox'>
                    <div className='unplayedBox-left'>
                        <span className='iconfonts icon-weiwancheng'></span>
                    </div>
                    <div className='unplayedBox-right'>
                        <span className='unplayedBox-number'>
                            {timeNumber(latestActivityData).createCount}
                        </span>
                        <p className='unplayedBox-title'>未开始的活动</p>
                    </div>
                    <div className='unplayedBox-look' onClick={() => handlOneLook(0)}>
                        <UnplayedBoxLook />
                    </div>
                </div>
            </div>
            <div className='underway'>
                <div className='underwayBox'>
                    <div className='underwayBox-left'>
                        <span className='iconfonts icon-duihao'></span>
                    </div>
                    <div className='underwayBox-right'>
                        <span className='underwayBox-number'>
                            {timeNumber(latestActivityData).conductCount}
                        </span>
                        <p className='underwayBox-title'>进行中的活动</p>
                    </div>
                    <div className='underwayBox-look' onClick={() => handlOneLook(1)}>
                        <UnplayedBoxLook />
                    </div>
                </div>
            </div>
            <div className='alreadyOver'>
                <div className='alreadyOverBox'>
                    <div className='alreadyOverBox-left'>
                        <span className='iconfonts icon-jieshu'></span>
                    </div>
                    <div className='alreadyOverBox-right'>
                        <span className='alreadyOverBox-number'>
                            {timeNumber(latestActivityData).overCount}
                        </span>
                        <p className='alreadyOverBox-title'>已结束的活动</p>
                    </div>
                    <div className='alreadyOverBox-look' onClick={() => handlOneLook(2)}>
                        <UnplayedBoxLook />
                    </div>
                </div>
            </div>
        </div>
    )
}
