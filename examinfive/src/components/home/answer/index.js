import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { instance } from '../../../service/request'
import { useNavigate } from 'react-router-dom'
import { Button, Switch, FloatButton } from 'antd';
// 异步的redux的方法
import { editData } from '../../../store/liststore'
import { Stepper } from 'react-vant'
import '../css/answer.css'
export default function Index() {
  // 注册路由
  const navigate = useNavigate()
  // react-redux注册方法
  const dispatch = useDispatch()
  // 题目数据的定义
  const [questionData, swtQuestionData] = useState([])
  // 第一长度
  const [firstLength, setFirstLength] = useState([])
  // 刷新的依赖
  const [flag, setFlag] = useState(false)
  // 显示答案
  const [flags, setFlags] = useState(false)
  // 改变下标获取题目
  // const [currentIndex, setCurrentIndex] = useState(0)
  // 擂主数据的获取
  const cLists = useSelector(state => state.lists.cLists)
  //  挑战者数据的获取
  const cgLists = useSelector(state => state.lists.cgLists)
  //  题目的获取
  async function getData() {
    let { question, code, question2 } = await instance.get('/rob/roblist')
    if (code === 200) {
      setFirstLength(question2)
      swtQuestionData(question)
    }
  }
  // 显示答案的内容
  // const handlePrev = () => {
  //   if (currentIndex > 0) {
  //     setCurrentIndex(currentIndex - 1);
  //     getData()
  //     setFlag(true)
  //   }
  // };
  // const handleNext = () => {
  //   if (currentIndex < firstLength.length - 1) {
  //     setCurrentIndex(currentIndex + 1);
  //     getData()
  //     setFlag(true)
  //   }
  // };
  // 改变分数
  const handlechangenum = (id, val) => {
    dispatch(editData({ id: id, val: val }))
  }
  const handleflushed = () => {
    setFlag(true)
  }
  useEffect(() => {
    getData()
  }, [flag])
  const hasRunOnce = sessionStorage.getItem('hasRunOnce') || '';
  useEffect(() => {
    let item;
    if (hasRunOnce === 'true') {
      item = setTimeout(() => {
        window.location.reload();
        sessionStorage.setItem('hasRunOnce', 'false');
      }, 300);
    }
    return () => {
      if (item) {
        clearTimeout(item);
      }
    };
  }, [hasRunOnce]);
  return (
    // console.log(hasRunOnce),
    <div>
      <div>{`\u00A0`}</div>
      <div className='headerblack'>
        <FloatButton
          shape="circle"
          type="primary"
          description='去PK'
          onClick={() => {
            navigate('/rob')
            sessionStorage.setItem('hasRunOnce', 'false');
          }}
          style={{
            right: 60,
            top: '10%'
          }}
        // icon={<CustomerServiceOutlined />}
        />
      </div>
      {/* 弹开答题和内容 */}
      {/* 第一层的题目 */}
      <div className='timubox'>
        <div>
          <h3>简答题</h3>
          {/* <h4 onClick={() => setFlag(false)}>刷新题目</h4> */}
          {/* <div>
            <Button onClick={handlePrev} disabled={currentIndex === 0} type="link"> 上一题</Button>
          </div>
          <div >
            <Button onClick={handleNext} disabled={currentIndex === firstLength.length - 1} type="link"> 下一题</Button>
          </div> */}
        </div>
        <div style={{ fontSize: '25px', fontWeight: '500' }}>
          {questionData.map(item => {
            return <div key={item._id}>
              <div className='tionebox'>
                <div>
                  {item.title}
                </div>
                <div >
                  {item.info.map((item2, index) => {
                    return <div key={index}>
                      {item.flag && <div>
                        {item2.title}
                      </div>}
                    </div>
                  })}
                </div>
                <div style={{ fontSize: '18px', margin: '1vw 0vw 0 3vw' }}>
                  答案
                  <Switch checked={flags} style={{ marginLeft: '0.5vw' }} onChange={() => setFlags(!flags)} />
                </div>
              </div>
            </div>
          })}
        </div>
        <div onClick={handleflushed}>
          <div>
            <Button style={{ fontSize: '20px' }} onClick={() => {
              getData()
              setFlags(false)
            }} type="link">刷新题目</Button>
          </div>
        </div>
      </div>
      {flags && <div style={{ letterSpacing: '4px', margin: '0 5vw', textAlign: 'left', textIndent: '2em', wordSpacing: '2px' }}>
        {questionData.map(item => {
          return <div key={item._id}>
            {item.info.map((item2, index) => {
              return <div key={index}>
                {item2.title}
              </div>
            })}
          </div>
        })}
      </div>}
      <div className='headerbox1'>
        <h3>
          姓名
        </h3>
        <h3>
          分数
        </h3>
        <h3 style={{ width: '5.5%' }}>
          班级
        </h3>
        <h3>
          按钮
        </h3>
      </div>
      {/* 第一层数据 */}
      <div className='namebox'>
        {/* 擂主的数据 */}
        <div style={{ margin: '2vw 0px' }}>
          {cLists.map(item => {
            return <div key={item._id}>
              <div >
                <div className='questionbox' style={{ margin: '2vw 0 0 0px' }}>
                  <div style={{ width: '5%' }}>{item.username}</div>
                  <div>{item.fraction}</div>
                  <div>{item.className}</div>
                  <div>
                    <Stepper max={100} min={0} value={item.fraction} step={1} onChange={val => handlechangenum(item._id, val)} />
                  </div>
                </div>
              </div>
            </div>
          })}
        </div>
        {/* 挑战者的数据 */}
        <div style={{ margin: '0vw 0vw 2vw 0px' }}>
          {cgLists.map(item => {
            return <div key={item._id}>
              <div>
                <div className='questionbox' style={{ margin: '0vw 0 2vw 0px' }}>
                  <div style={{ width: '5%' }}>{item.username}</div>
                  <div>{item.fraction}</div>
                  <div>{item.className}</div>
                  <div>  <Stepper max={100} min={0} value={item.fraction} step={1} onChange={val => handlechangenum(item._id, val)} /> </div>
                </div>
              </div>
            </div>
          })}
        </div>
      </div>
      <div style={{ float: 'right', marginRight: '8.6vw' }}>
        {/* <Button style={{ fontSize: '20px' }} onClick={() => xuanxin()} type="link">刷新人名</Button> */}
      </div>
    </div>
  )
}
