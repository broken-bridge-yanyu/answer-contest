import React from 'react';
import './index.css'

export default function Index() {
  const redirectToURL = () => {
    window.location.href = 'https://xinghuo.xfyun.cn/?ch=bdtg_xh_wpm&bd_vid=7921070217416326569';
  };


  return (
    <div className='xf-container'>
      <div>
        <h2 className='h2-c'>我是讯飞星火认知大模型</h2>
        <p className='p1-c'><r className='r'>只需一个指令</r>“懂你所言，答你所问，创你所需，解你所难，学你所教”</p>
        <p className='p2-c'>奇思妙想，我是你的创意搭档</p>
      </div>
      <button className='c-button' onClick={redirectToURL}>立即使用</button>
    </div>
  );
}
