import React, { useEffect, useState } from "react";
import { message, InputNumber, Button } from "antd";
import Raceto from "./raceto";
import { instance } from "../../../service/request";
import "./raceto.css";

export default function Index() {
  const [userData, setUserData] = useState([]);
  const [localUserData, setLocalUserData] = useState([]); // 本地缓存状态
  const [showTopThree, setShowTopThree] = useState(false);
  const [disableScoreChange, setDisableScoreChange] = useState(false);
  const [racetoVisible, setRacetoVisible] = useState(false);
  const [racetoKey, setRacetoKey] = useState(0);

  const getData = async () => {
    try {
      const { code, data1 } = await instance.get("/rob/roblist");
      if (code === 200) {
        // 验证本地缓存的 username 是否一致
        const isUsernameConsistent = validateUsernameConsistency(data1);
        if (isUsernameConsistent) {
          setUserData(data1);
          setLocalUserData(data1); // 更新本地缓存
          localStorage.setItem("renderData", JSON.stringify(data1)); // 更新本地存储
          console.log("成功获取数据：", data1);
        } else {
          console.warn(
            "本地缓存中的 username 与从服务器获取的数据中的 username 不一致。使用服务器数据。"
          );
          setUserData(data1);
          setLocalUserData(data1);
        }
      } else {
        console.error("获取数据错误 - 服务器返回非 200 状态");
      }
    } catch (error) {
      console.error("获取数据时发生错误", error);
    }
  };

  // 在组件初始化时从本地存储读取数据
  useEffect(() => {
    const storedData = localStorage.getItem("renderData");
    if (storedData) {
      setLocalUserData(JSON.parse(storedData));
    } else {
      // 如果本地存储没有数据，则从服务器获取
      getData();
    }
  }, []);

  // 当 disableScoreChange 变为 false 且本地没有缓存数据时，从服务器获取数据
  useEffect(() => {
    if (!disableScoreChange && localUserData.length === 0) {
      getData();
    }
  }, [disableScoreChange, localUserData]);
  // 在 handleFractionChange 中保存到本地存储
  const handleFractionChange = (id, newValue) => {
    if (!disableScoreChange) {
      setUserData((prevData) => {
        const updatedData = prevData.map((item) =>
          item._id === id ? { ...item, RobFraction: newValue } : item
        );
        // 如果本地缓存存在，也更新本地缓存
        setLocalUserData(updatedData);
        localStorage.setItem("renderData", JSON.stringify(updatedData));
        return updatedData;
      });
    } else {
      message.warning("分数变更已禁用，请计算排名！");
    }
  };

  // 验证本地缓存的 username 是否一致
  const validateUsernameConsistency = (serverData) => {
    if (localUserData.length !== serverData.length) {
      return false;
    }
    for (let i = 0; i < localUserData.length; i++) {
      if (localUserData[i].username !== serverData[i].username) {
        return false;
      }
    }
    return true;
  };

  const calculateRanking = () => {
    const sortedData = [...userData].sort(
      (a, b) => b.RobFraction - a.RobFraction
    );
    const topThree = sortedData.slice(0, 3);
    setUserData(topThree);
    setShowTopThree(true);
    setDisableScoreChange(true);
  };

  const endRace = () => {
    setDisableScoreChange(true);
    setRacetoVisible(false);
  };

  const startRace = () => {
    setDisableScoreChange(false);
    setRacetoKey((prevKey) => prevKey + 1);
    setRacetoVisible(true);
  };

  const resetScores = () => {
    const newDataWithZeroFraction = localUserData.map((item) => ({
      ...item,
      RobFraction: 0,
    }));
    setUserData(newDataWithZeroFraction);
    setLocalUserData(newDataWithZeroFraction); // 修正本地缓存
    localStorage.setItem("renderData", JSON.stringify(newDataWithZeroFraction));
  };

  // 在组件渲染时，使用本地缓存的数据
  const renderData = localUserData.length > 0 ? localUserData : userData;

  return (
    <div>
      {!showTopThree ? (
        <>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button
              onClick={startRace}
              style={{
                width: "20%",
                height: "100px",
                background: "#52c41a",
                color: "#fff",
                fontSize: "24px",
                lineHeight: "100px",
                borderRadius: "25px",
                border: "none",
                marginRight: "10px",
                padding: "0 20px",
                transition: "background 0.3s ease",
              }}
            >
              开始抢答
            </Button>

            <Button
              onClick={endRace}
              style={{
                width: "20%",
                height: "100px",
                background: "#ff4d4f",
                color: "#fff",
                fontSize: "24px",
                lineHeight: "100px",
                borderRadius: "25px",
                padding: "0 20px",
              }}
            >
              结束抢答
            </Button>

            <Button
              onClick={calculateRanking}
              style={{
                width: "20%",
                height: "100px",
                background: "#ffd700",
                color: "#fff",
                fontSize: "24px",
                lineHeight: "100px",
                borderRadius: "25px",
                padding: "0 20px",
              }}
            >
              计算排名
            </Button>

            <Button
              onClick={resetScores}
              style={{
                width: "20%",
                height: "100px",
                background: "#1890ff",
                color: "#fff",
                fontSize: "24px",
                lineHeight: "100px",
                borderRadius: "25px",
                padding: "0 20px",
              }}
            >
              分数清零
            </Button>
          </div>
          {racetoVisible && <Raceto key={racetoKey} />}
          <div className="headerbox1">
            <h3 className="header-item">姓名</h3>
            <h3 className="header-item">分数</h3>
            <h3 className="header-item" style={{ width: "5.5%" }}>
              班级
            </h3>
            <h3 className="header-item">按钮</h3>
          </div>
          <div className="namebox">
            <div style={{ margin: "2vw 0px" }}>
              {renderData.map((item) => (
                <div key={item._id}>
                  <div>
                    <div
                      className="questionbox"
                      style={{ margin: "2vw 0 0 0px" }}
                    >
                      <div style={{ width: "5%" }}>{item.username}</div>
                      <div>{item.RobFraction}</div>
                      {/*!!! 检查item是否有'college'数组，且长度大于2 !!!*/}
                      {item.college && item.college.length > 2 && (
                        <div>{item.college[2].collegeName}</div>
                      )}
                      <div>
                        <InputNumber
                          min={0}
                          max={100}
                          step={1}
                          value={item.RobFraction}
                          
                          onChange={(value) =>
                            handleFractionChange(item._id, value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </>
      ) : (
        <>
          <Button
            onClick={() => setShowTopThree(false)}
            style={{
              position: "absolute",
              top: "10px",
              left: "10px",
              padding: "5px 10px",
              fontSize: "16px",
            }}
          >
            返回抢答页
          </Button>
          <div className="winner-container top-three">
            {userData.map((item, index) => (
              <div
                key={item._id}
                className={`winner-box winner-${index + 1} ${
                  index === 1 ? "winner-center" : ""
                }`}
              >
                <span className="winner-rank">{index + 1}</span>
                <span className="winner-username">{item.username}</span>
                <span className="winner-fraction">{item.RobFraction} 分</span>
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
}
