import React, { useState, useEffect } from "react";
import { Switch } from "antd";
import { instance } from '../../../service/request'
import "./raceto.css"; // 引入样式文件

const Raceto = () => {
  const [showQuestion, setShowQuestion] = useState(false);
  const [showAnswer, setShowAnswer] = useState(false);
  const [currentQuestion, setCurrentQuestion] = useState();
  const [questionsAnswered, setQuestionsAnswered] = useState(0);
  // 获取题目的逻辑
  async function fetchQuestion() {
    let { question, code } = await instance.get("/rob/roblist");
    if (code === 200 && question.length > 0) {
      // 处理获取到的题目
      const retrievedQuestion = question[0];
      setCurrentQuestion({
        title: retrievedQuestion.title,
        info: retrievedQuestion.info,
      });
      setShowAnswer(false);
      setShowQuestion(true);
    }
  }

  const startCountdown = () => {
    fetchQuestion(); // 获取新题目的逻辑
    setQuestionsAnswered((prev) => prev + 1);
  };

  const refreshQuestion = () => {
    fetchQuestion(); // 获取新题目的逻辑
    setShowAnswer(false);
  };

  const handleSwitchChange = (checked) => {
    if (showQuestion) {
      if (checked) {
        setQuestionsAnswered((prev) => prev + 1);
      } else {
        setQuestionsAnswered((prev) => Math.max(0, prev - 1));
      }
      setShowAnswer(checked);
    }
  };

  useEffect(() => {
    if (questionsAnswered === 3) {
      setShowQuestion(false);
    }
  }, [questionsAnswered]);

  return (
    <div className="raceto-container">
      <>
        <button
          className="raceto-button"
          onClick={() => (showQuestion ? refreshQuestion() : startCountdown())}
          style={{
            padding: "10px 20px",
            fontSize: "16px",
            backgroundColor: showQuestion ? "#ff6347" : "#4caf50",
            color: "#fff",
            border: "none",
            borderRadius: "5px",
            cursor: "pointer",
            boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
            transition: "background-color 0.3s ease",
          }}
        >
          {showQuestion ? "刷新题目" : "点击展示题目"}
        </button>

        {showQuestion && (
          <div className="raceto-question-container">
            <h3 className="raceto-question-title">{currentQuestion?.title}</h3>

            <div className="raceto-answer-container">
              <span className="raceto-answer-label">答案：</span>
              <Switch checked={showAnswer} onChange={handleSwitchChange} />
            </div>
            {showAnswer && (
              <h3 className="raceto-answer">{`${currentQuestion?.info[0]?.title}`}</h3>
            )}
          </div>
        )}
      </>
    </div>
  );
};

export default Raceto;
