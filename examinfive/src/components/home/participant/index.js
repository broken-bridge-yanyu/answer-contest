import React, { memo, useState, useEffect, useRef, useCallback } from 'react';
import { Space, Table, Button, Input, Modal, Form, Upload, Radio, Select, message, Cascader } from 'antd';
import { NavLink } from 'react-router-dom';
import { instance } from '../../../service/request'
import { GetParticipantApi } from '../../../service/index'
import _ from 'lodash';
import './index.css';
import * as XLSX from 'xlsx'
const { Option } = Select;

//处理学院的数据转换成以下类型
const transformDataForCascader = (data) => {
  return data.map((item) => ({
    value: item._id,
    label: item.collegeName,
    children: item.children ? transformDataForCascader(item.children) : [],
  }))
}
const Index = memo(() => {
  const inputRef = useRef();
  const debounce = _.debounce;
  const [tableData, setTableData] = useState([]) // 表格数据
  const [tableFile, setTableFile] = useState() // 文件
  const [sheetNames, setSheetNames] = useState([]) // 工作表列表
  const [defaultSheetName, setDefaultSheetName] = useState('') // 默认工作表
  const handleUpload = (file, sheetName) => {

    // 异步读取文件
    const reader = new FileReader()
    reader.onload = async (e) => {
      // 格式化数组
      const data = new Uint8Array(e.target.result)
      // 读取文件内容
      const workbook = XLSX.read(data, { type: 'array' })
      // 如果指定了 sheetName，则将其用于查找工作表；否则，使用默认的第一个工作表。
      const worksheet = sheetName ? workbook.Sheets[sheetName] : workbook.Sheets[workbook.SheetNames[0]]
      // 将工作表数据转换为 JSON 格式
      const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 })
      console.log(jsonData, 'json数据');
      //处理导入的excel文件变成[{},{}]格式
      if (jsonData.length > 0) {
        const newData = [];
        // 第一行默认认为是表头 除开第一行以外都是数据
        jsonData.slice(1).forEach((item, itemIndex) => {
          if (item.length === 0) return false;
          const obj = { key: itemIndex };
          jsonData[0].forEach((header, index) => {
            obj[header] = item[index];
          });
          newData.push(obj);
        });

        // 设置数据
        const datas = await instance.post('/personnel/addExcel', newData)
        // console.log(newData, '新的数据');

      }
    }
    reader.readAsArrayBuffer(file)
  }
  const tableDataImport = () => {

  }

  // 需要参数  value:工作区
  const handleChange = (value) => {
    // 调用数据渲染的方法 传入文件以及工作区
    if (tableFile) handleUpload(tableFile, value)
  }

  const columns = [
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '性别',
      dataIndex: 'gender',
      key: 'gender',
    },
    {
      title: '学院',
      dataIndex: 'college',
      key: 'college',
      render: (_, data) => (
        <span>{data?.college[0]?.collegeName}</span>
      ),
    },
    {
      title: '阶段',
      dataIndex: 'college',
      key: 'college',
      render: (_, data) => (
        <span>{data?.college[1]?.collegeName}</span>
      ),
    },
    {
      title: '班级',
      dataIndex: 'className',
      key: 'college',
      render: (_, data) => (
        <span>{data?.college[2]?.collegeName}</span>
      ),
    },
    {
      title: '操作',
      key: 'action',
      render: (_, item) => (
        <Space size="middle">
          <p onClick={() => { edit(item) }} >编辑</p>
          <a
            style={{ color: '#ff903d' }}
            onClick={() => {
              const shouldDelete = window.confirm('确定删除吗？');
              if (shouldDelete) {
                instance.get(`/personnel/del?id=${item._id}`).then((val) => {
                  if (val) {
                    setflag(true)
                  }
                }).catch((error) => {
                  console.error('删除请求失败：', error);
                });
              }
            }}
          >
            删除
          </a>

        </Space>
      ),
    },
  ];

  const [datas, setDatas] = useState('')
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 获取修改的数据id
  const [editDataid, setEditDataid] = useState('')
  const [flag, setflag] = useState(false)
  const handleOk = () => {
    instance.put(`/personnel/edit/${editDataid}`, form.getFieldValue()).then(({ code }) => {
      if (code === 200) {
        setflag(true)
      }
    }).catch((error) => {
      console.log(error);
    })
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // 页面初次渲染
  let [data, setData] = useState([]);
  useEffect(() => {
    setflag(false)
    instance.get('/personnel/list').then(val => {
      // console.log(val.data);
      setData(val.data);
    });
    CollegeList()
  }, [flag]);
  // 筛选套餐
  const select = useCallback(
    debounce((val, type) => {
      instance.get(`/personnel/list?username=${inputRef.current.input.value}`).then(val => {
        setData(val.data.data);
      });
    }, 300),
    [],
  );

  // 存储选中的数据
  const [rowData, setRowData] = useState([]);
  const rowSelection = {
    selectedRowKeys: rowData,
    onChange: (selectedRowKeys) => {
      setRowData(selectedRowKeys);
    },
  };

  // 批量删除
  const alldel = () => {
    rowData.forEach((item) => {
      instance.get(`/personnel/del?id=${item}`).then(val => {
        setData(val.data.data);
      });
    });
  };

  //回填数据
  const [form] = Form.useForm()
  //学院/阶段/班级
  let [collegeList, setCollegeList] = useState([])
  const CollegeList = () => {
    GetParticipantApi('/personnel/collegeList').then(res => {
      let { code, data } = res
      if (code === 200) {
        setCollegeList(data)
      }
    })
  }
  //数据修改回填
  const edit = (item) => {
    const collegeValues = item.college.map((collegeItem) => collegeItem._id);
    form.setFieldsValue({
      id: item._id,
      username: item.username,
      gender: item.gender,
      age: item.age,
      college: collegeValues,//学院/阶段/班级
    })
    setEditDataid(item._id)
    setIsModalOpen(true)
  }
  // console.log(data, '··········表格数据········');
  const handelsearch = (e) => {
    if (e.keyCode === 13) {
      setDatas(e.target.value)
    }
    if (e.target.value === '') {
      setflag(true)
      setDatas('')
    }
  }
  return (
    <div>
      <div className="fileUpDiv" style={{ marginTop: 10 }}>
        <Upload
          beforeUpload={file => {
            const isExcel = /\.(xlsx|xls)$/.test(file.name.toLowerCase())
            if (!isExcel) {
              message.error('请上传Excel文件（.xlsx 或 .xls）')
              return false
            }
            // 保存上传的文件
            setTableFile(file)
            // 调用数据渲染的方法
            handleUpload(file)
            return false
          }}>
          <Button type="primary">批量添加人员</Button>
        </Upload>
      </div>
      <p>
        <Input placeholder="请输入参赛人员名称" className="selects" ref={inputRef} onKeyDown={(e) => { handelsearch(e) }} />
        <a
          style={{ color: '#f56c6c', marginRight: '10px', marginLeft: '10px' }}
          onClick={() => {
            const shouldDelete = window.confirm('确定批量删除吗？');
            if (shouldDelete) {
              alldel(); // 执行批量删除操作
            }
          }}
        >
          批量删除
        </a>
        <Button type="primary" style={{ color: '#fff', background: '#f56c6c' }}>
          <NavLink to="/home/participant/add">添加参赛成员</NavLink>
        </Button>
      </p>
      <Table columns={columns} dataSource={datas !== '' ? data.filter(item => item.username.includes(datas)) : data} rowKey={'_id'} rowSelection={rowSelection} pagination={{
        defaultPageSize: 5,
      }} />
      <Modal title="修改人员" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <Form
          name="memberForm"
          labelCol={{ flex: '110px' }}
          labelAlign="left"
          labelWrap
          wrapperCol={{ flex: 1 }}
          colon={false}
          style={{ maxWidth: 600 }}
          form={form}
        >
          <Form.Item label="姓名" name="username" rules={[{ required: true, message: '请输入姓名' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="性别" name="gender" rules={[{ required: true, message: '请选择性别' }]}>
            <Radio.Group>
              <Radio value="男">男</Radio>
              <Radio value="女">女</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item label="年龄" name="age" rules={[{ required: true, message: '请输入年龄' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="学院" name="college" rules={[{ required: true, message: '请输入学院' }]}>
            {/* <Input /> */}
            <Cascader options={transformDataForCascader(collegeList)} value={form.getFieldValue('college')} placeholder="学院/班级" />
          </Form.Item>
          {/* <Form.Item label="阶段" name="stage" rules={[{ required: true, message: '请输入阶段' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="班级" name="className" rules={[{ required: true, message: '请选择班级' }]}>
            <Select>
              <Option value="2302A">2302A</Option>
              <Option value="2212A">2212A</Option>
            </Select>
          </Form.Item> */}
        </Form>
      </Modal>
    </div>
  );
});

export default Index;
