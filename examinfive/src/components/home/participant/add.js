import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Radio, Select, Cascader } from 'antd';
import { useNavigate } from 'react-router-dom';
import { GetParticipantApi, PostParticipantApi } from '../../../service/index'
import ReturnTopBtn from '../../../function/functionBox/vote/returnTopBtn/returnTopBtn'
const { Option } = Select;
const App = () => {
  useEffect(() => {
    CollegeList()
  }, [])
  const navigate = useNavigate();
  const onFinish = (values) => {
    PostParticipantApi('/personnel/add', values)
    navigate('/home/participant');
  };
  // const beforeUpload = (file) => {
  //   const isImage = file.type.startsWith('image/');
  //   if (!isImage) {
  //     message.error('请上传图片文件！');
  //   }
  //   return isImage;
  // };
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };
  const customRequest = ({ onSuccess, onError, file }) => {
    setTimeout(() => {
      onSuccess();
    }, 1000);
  };
  //学院/阶段/班级
  let [collegeList, setCollegeList] = useState([])
  const CollegeList = () => {
    GetParticipantApi('/personnel/collegeList').then(res => {
      let { code, data } = res
      if (code === 200) {
        setCollegeList(data)
      }
    })
  }
  //处理学院的数据转换成以下类型
  const transformDataForCascader = (data) => {
    return data.map((item) => ({
      value: item._id,
      label: item.collegeName,
      children: item.children ? transformDataForCascader(item.children) : [],
    }))
  }
  // const cascaderOptions = transformDataForCascader(collegeList)
  const onChange = (value) => {
    console.log(value);
  };
  return (
    <div>
      <ReturnTopBtn pathNavigate={'/home/participant'} />
      <Form
        name="memberForm"
        labelCol={{ flex: '110px' }}
        labelAlign="left"
        labelWrap
        wrapperCol={{ flex: 1 }}
        colon={false}
        style={{ maxWidth: 600 }}
        onFinish={onFinish}
      >
        <Form.Item label="姓名" name="username" rules={[{ required: true, message: '请输入姓名' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="性别" name="gender" rules={[{ required: true, message: '请选择性别' }]}>
          <Radio.Group>
            <Radio value="男">男</Radio>
            <Radio value="女">女</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="年龄" name="age" rules={[{ required: true, message: '请输入年龄' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="学院/阶段/班级" name="college" rules={[{ required: true, message: '请输入学院' }]}>
          <Cascader options={transformDataForCascader(collegeList)} onChange={onChange} placeholder="学院/班级" />
        </Form.Item>
        {/* <Form.Item label="班级" name="className" rules={[{ required: true, message: '请输入班级' }]}>
        <Input />
      </Form.Item> */}
        {/* <Form.Item
        label="文件上传"
        name="files"
        valuePropName="fileList"
        getValueFromEvent={normFile}
        rules={[{ message: '请上传文件' }]}
      >
        <Upload
          name="files"
          multiple={true}
          customRequest={customRequest}
          showUploadList={false}
        >
          <Button icon={<UploadOutlined />}>点击上传文件</Button>
        </Upload>
      </Form.Item> */}
        <Form.Item label="个人描述" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item label=" ">
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default App;
