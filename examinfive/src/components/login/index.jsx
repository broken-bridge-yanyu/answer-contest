import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom"
import { LoginAPI } from "../../service/index"
import "./login.css";

import { fn } from '../message/index'
export default function Index(props) {
  let Navgates = useNavigate();
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  let usererrmessage = useRef();
  let passworderrmesage = useRef();
  const Login = () => { 
    if (username && password) {
      usererrmessage.current.style.display = "none";
      passworderrmesage.current.style.display = "none";
      localStorage.setItem('userId', JSON.stringify(username))
      LoginAPI().login({
        username, password
      }).then((res) => {
        fn(res.code, res.msg)
        localStorage.setItem("user", JSON.stringify(res.user));
        localStorage.setItem("accessToken", res.accessToken);
        localStorage.setItem("refreshToken", res.refreshToken);
        Navgates("/home")
      })
    } else {
      if (username === "") {
        usererrmessage.current.style.display = "block";
        usererrmessage.current.innerHTML = "请输入用户名";
        console.log(usererrmessage);
      }
      if (password === "") {
        passworderrmesage.current.style.display = "block";
        passworderrmesage.current.innerHTML = "请输入密码";
      }
    }
  };
  return (
    <div className="form_box">

      <div className="form_center">
        <div className="form_left"></div>
        <div className="form_right">
          {/* <img
            src={icon01}
            alt=""
          ></img> */}
          <div className="form">
            <div className="form_item">
              <div className="form_item_center">
                <span>用户名</span>
                <input
                  type="text"
                  value={username}
                  placeholder="请输入用户名"
                  onChange={(e) => {
                    usererrmessage.current.style.display = "none";

                    setusername(e.target.value);
                  }}
                />
              </div>
              <p className="err errusername" ref={usererrmessage}>
                错误提示
              </p>
            </div>
            <div className="form_item">
              <div className="form_item_center">
                <span>密码</span>
                <input
                  type="password"
                  value={password}
                  placeholder="请输入密码"
                  onChange={(e) => {
                    passworderrmesage.current.style.display = "none";
                    setpassword(e.target.value);
                  }}
                />
              </div>
              <p className="err errpassword" ref={passworderrmesage}>
                错误提示
              </p>
            </div>
            <button className="loginbtn" onClick={Login}>
              登录
            </button>
          </div>
        </div >

      </div >
      <a className="bean" href="https://beian.miit.gov.cn/">ICP备案号:冀ICP备2023043377号   <span className="development">开发组成员：马洪伟、夏亚龙、王立阳、王龙波、张博隆、张天宇、王子杰</span>  <span className="version">版本号:2.0</span></a>
    </div >
  );
}
