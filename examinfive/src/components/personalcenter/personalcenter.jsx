import React, { useState } from 'react';
import { LaptopOutlined, NotificationOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Menu, theme} from 'antd';
import {  Outlet, useNavigate } from 'react-router-dom';



const { Header, Content, Sider } = Layout;
const items1 = ['1'].map((key) => ({
  key,
  label: `个人中心`,
}));
const items2 = [UserOutlined, LaptopOutlined, NotificationOutlined].map((icon, index) => {
  const key = String(index + 1);
  return key === '1'
    ? {
      key: '/personalcenter/jieshao',
      label: '个人介绍',
    }
    : key === '2'
      ? {
        key: '/personalcenter/shoucang',
        label: '个人收藏',
      }
      : null;
});


  const App = () => {
    // 跳到收藏
    const navigate = useNavigate();
    //  表单
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
      setIsModalVisible(true);
    };

    const handleOk = () => {
      setIsModalVisible(false);
    };

    const handleCancel = () => {
      setIsModalVisible(false);
    };

    const onFinish = (values) => {
      console.log('收到表单值:', values);
      // 在这里处理表单提交逻辑
      setIsModalVisible(false);
    };
    const onMenuChange=(item)=>{
      navigate(item.key)
    }

    // 整体
    const [componentSize, setComponentSize] = useState('default');
    const onFormLayoutChange = ({ size }) => {
      setComponentSize(size);
    };

    const {
      token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();

    return (
      <Layout>
        <Header
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <div className="demo-logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            items={items1}
            style={{
              flex: 1,
              minWidth: 0,
            }}
          />
        </Header>
        <Layout>
          <Sider
            width={200}
            style={{
              background: colorBgContainer,
            }}
          >
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{
                height: '100%',
                borderRight: 0,
              }}
              onClick={(item)=>{onMenuChange(item)}}
              items={items2}
            />
          </Sider>
          {/* 渲染二级路由 */}
            <div style={{width:'100%'}}>
            <Outlet/> 
            </div>
        </Layout>
      </Layout>
    );
  };
  export default App;