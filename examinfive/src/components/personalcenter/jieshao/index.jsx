import React, { useState } from 'react';
import { UserOutlined } from '@ant-design/icons';
import {
  Breadcrumb, Layout, theme, Button,
  Form,
  Input,
  Modal,
  Radio,
  Avatar,
} from 'antd';

const { Header, Content, Footer } = Layout;
export default function Index() {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
      setIsModalVisible(true);
    };

    const handleOk = () => {
      setIsModalVisible(false);
    };

    const handleCancel = () => {
      setIsModalVisible(false);
    };

    const onFinish = (values) => {
      console.log('收到表单值:', values);
      // 在这里处理表单提交逻辑
      setIsModalVisible(false);
    };
    const onMenuChange=(item)=>{
      console.log(item.key,'aaaa');
    }

    // 整体
    const [componentSize, setComponentSize] = useState('default');
    const onFormLayoutChange = ({ size }) => {
      setComponentSize(size);
    };

    const {
      token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();
    
  return (
    <div>
         <Layout
            style={{
              padding: '0 24px 24px',
            }}
          >
            <Breadcrumb
              style={{
                margin: '16px 0',
              }}
            >
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>My</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
              }}
            >
              <Form
                labelCol={{
                  span: 4,
                }}
                wrapperCol={{
                  span: 14,
                }}
                layout="horizontal"
                initialValues={{
                  size: componentSize,
                }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
                style={{
                  maxWidth: 600,
                }}
              >
                <Form.Item label="Form Size" name="size">
                  <Radio.Group>
                    <Radio.Button value="small">Small</Radio.Button>
                    <Radio.Button value="default">Default</Radio.Button>
                    <Radio.Button value="large">Large</Radio.Button>
                  </Radio.Group>
                </Form.Item>

                <div style={{ textAlign: 'center', padding: '20px' }}>
                  <Avatar size={64} icon={<UserOutlined />} />
                  <h2>用户姓名</h2>

                  <Form
                    name="personalCenterForm"
                    onFinish={onFinish}
                    style={{ width: '300px', margin: '20px auto' }}
                  >
                    <Form.Item label="账号">
                      <Input disabled defaultValue="user_account" />
                    </Form.Item>
                    <Form.Item label="密码" name="password">
                      <Input.Password />
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" htmlType="submit">
                        保存更改
                      </Button>
                      <Button style={{ marginLeft: '10px' }} onClick={showModal}>
                        修改
                      </Button>
                    </Form.Item>
                  </Form>

                  <Modal
                    title="修改密码"
                    visible={isModalVisible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                  >
                    <Form name="modifyPasswordForm" onFinish={onFinish}>
                      <Form.Item
                        label="新密码"
                        name="newPassword"
                        rules={[
                          {
                            required: true,
                            message: '请输入新密码',
                          },
                        ]}
                      >
                        <Input.Password />
                      </Form.Item>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          确定
                        </Button>
                      </Form.Item>
                    </Form>
                  </Modal>
                </div>
              </Form>

            </Content>
          </Layout>
    </div>
  )
}
