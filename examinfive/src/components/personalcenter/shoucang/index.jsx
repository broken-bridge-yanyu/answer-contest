import React, { useState, useEffect } from 'react';
import { FloatButton, List, Button, Input, Select, Pagination } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const { Option } = Select;

const PersonalCollection = () => {
  const [searchText, setSearchText] = useState('');
  const [category, setCategory] = useState('all');
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [totalItems, setTotalItems] = useState(0);
  const [collectionItems, setCollectionItems] = useState([]);

  // 模拟从后端获取个人收藏数据
  useEffect(() => {
    // 这里可以替换成实际的 API 调用
    const fetchData = async () => {
      // 模拟数据
      const data = [
        { id: 1, title: '收藏项 1', category: 'category1' },
        { id: 2, title: '收藏项 2', category: 'category2' },
        // 更多数据...
      ];

      // 模拟分页
      const startIndex = (currentPage - 1) * pageSize;
      const endIndex = startIndex + pageSize;
      const slicedData = data.slice(startIndex, endIndex);

      setTotalItems(data.length);
      setCollectionItems(slicedData);
    };

    fetchData();
  }, [currentPage, pageSize]);

  // 处理搜索和筛选
  const handleSearch = (value) => {
    setSearchText(value);
    setCurrentPage(1); // 重置到第一页
  };

  const handleCategoryChange = (value) => {
    setCategory(value);
    setCurrentPage(1); // 重置到第一页
  };

  return (
    <div style={{ padding: '20px', maxWidth: '800px', margin: 'auto' }}>
      <FloatButton title="个人收藏" subTitle="管理您的收藏" />
      <div style={{ marginBottom: '20px' }}>
        <Input.Search
          placeholder="搜索收藏项"
          allowClear
          enterButton
          value={searchText}
          onChange={(e) => handleSearch(e.target.value)}
        />
        <Select
          style={{ marginLeft: '10px', width: '150px' }}
          placeholder="选择分类"
          value={category}
          onChange={handleCategoryChange}
        >
          <Option value="all">全部分类</Option>
          <Option value="category1">分类1</Option>
          <Option value="category2">分类2</Option>
          {/* 更多分类选项... */}
        </Select>
      </div>
      <List
        dataSource={collectionItems}
        renderItem={(item) => (
          <List.Item>
            <List.Item.Meta
              avatar={<UserOutlined />}
              title={<a href="#">{item.title}</a>}
              description={`分类：${item.category}`}
            />
          </List.Item>
        )}
      />
      <div style={{ textAlign: 'center', marginTop: '20px' }}>
        <Pagination
          current={currentPage}
          pageSize={pageSize}
          total={totalItems}
          onChange={(page) => setCurrentPage(page)}
        />
      </div>
    </div>
  );
};

export default PersonalCollection;
