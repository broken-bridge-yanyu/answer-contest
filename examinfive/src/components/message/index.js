import { message } from 'antd';
const fn = (code, msg) => {
    if (code == 200) message.success(msg);//成功
    else if (code == 400) message.error(msg);//失败
    else message.warning('This is a warning message');
}
export { fn };
