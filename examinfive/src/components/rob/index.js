import React, { memo, useState, useEffect } from 'react'
import { Table, Button } from 'antd';
import { instance } from '../../service/request'
import './rob.css'
import { useDispatch } from 'react-redux'
import { getList } from '../../store/liststore'
import { useNavigate } from 'react-router-dom'
export default memo(function Rob() {
    // 注册navigate 
    const navigate = useNavigate()
    // 往redux里面传递数据
    const dispatch = useDispatch()
    const currryPk = () => {
        // 存入redux
        dispatch(getList({ champion: selectedRowKeys1, challenger: selectedRowKeys }))
        navigate('/home/answer')
        sessionStorage.setItem('hasRunOnce', 'true');
        // 跳转到比赛页面
    }
    // 挑战者定义全选反选的数据
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const onSelectChange = (newSelectedRowKeys) => {
        // console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };
    // 擂主定义全选反选
    const [selectedRowKeys1, setSelectedRowKeys1] = useState([]);
    const onSelectChange1 = (newSelectedRowKeys) => {
        // console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys1(newSelectedRowKeys);
    };
    // 表单一的内容，是挑战者
    const [data, setData] = useState([])
    // 表单二的内容是擂主
    const [datas1, setData1] = useState([])
    // 点击刷新挑战者
    // const [flag, setFlag] = useState(false)
    // 获取挑战者的方法
    async function getData() {
        let { data, code } = await instance.get('/rob/roblist')
        if (code === 200) {
            let datas = data.map(item => {
                item.key = item._id
                return item
            })
            setData(datas)
            console.log(data);
        }
    }
    // 获取擂主的方法
    async function getData1() {
        let { code, data1 } = await instance.get('/rob/roblist')
        if (code === 200) {
            let datas1 = data1.map(item => {
                item.key = item._id
                return item
            })
            setData1(datas1)
        }
    }
    // 获取挑战者的数据
    useEffect(() => {
        getData()
        // 点击刷新的依赖
    }, [])
    // 获取擂主的数据
    useEffect(() => {
        getData1()
    }, [])
    const columns = [
        {
            title: '姓名',
            dataIndex: 'username',
            width: 180,
            // 排序
            sorter: (a, b) => a.fraction - b.fraction
        },
        {
            title: '学院',
            dataIndex: 'college',
            key: 'college',
            render: (_, data) => (
                <span>{data?.college[0]?.collegeName}</span>
            ),
        },
        {
            title: '阶段',
            dataIndex: 'college',
            key: 'college',
            render: (_, data) => (
                <span>{data?.college[1]?.collegeName}</span>
            ),
        },
        {
            title: '班级',
            dataIndex: 'className',
            key: 'college',
            render: (_, data) => (
                <span>{data?.college[2]?.collegeName}</span>
            ),
        },
        {
            title: '分数',
            dataIndex: 'fraction',
        },
    ];
    // const data = [];
    // for (let i = 0; i < 46; i++) {
    //     data.push({
    //         key: i,
    //         name: `Edward King ${i}`,
    //         age: 32,
    //         address: `London, Park Lane no. ${i}`,
    //     });
    // }
    // 挑战者点击全选反选
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
        selections: [
            {
                key: 'all',
                text: '全选',
                onSelect: (changeableRowKeys) => {
                    // console.log('all', changeableRowKeys);
                    setSelectedRowKeys(changeableRowKeys);
                },
            },
            {
                key: 'invert',
                text: '反选',
                onSelect: (changeableRowKeys) => {
                    // console.log('invert', changeableRowKeys);
                    setSelectedRowKeys([]);
                },
            },
            // Table.SELECTION_ALL,
            // Table.SELECTION_INVERT,
            // Table.SELECTION_NONE,
            {
                key: 'odd',
                text: '选择第一个',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return false;
                        }
                        return true;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
            {
                key: 'even',
                text: '选中第二个',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return true;
                        }
                        return false;
                    });
                    setSelectedRowKeys(newSelectedRowKeys);
                },
            },
        ],
    };
    // 擂主点击全选反选
    const rowSelection1 = {
        selectedRowKeys1,
        onChange: onSelectChange1,
        selections: [
            {
                key: 'all',
                text: '全选',
                onSelect: (changeableRowKeys) => {
                    // console.log('all', changeableRowKeys);
                    setSelectedRowKeys1(changeableRowKeys);
                },
            },
            {
                key: 'invert',
                text: '反选',
                onSelect: (changeableRowKeys) => {
                    // console.log('invert', changeableRowKeys);
                    setSelectedRowKeys1([]);
                },
            },
            // Table.SELECTION_ALL,
            // Table.SELECTION_INVERT,
            // Table.SELECTION_NONE,
            {
                key: 'odd',
                text: '选择第一个',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return false;
                        }
                        return true;
                    });
                    setSelectedRowKeys1(newSelectedRowKeys);
                },
            },
            {
                key: 'even',
                text: '选中第二个',
                onSelect: (changeableRowKeys) => {
                    let newSelectedRowKeys = [];
                    newSelectedRowKeys = changeableRowKeys.filter((_, index) => {
                        if (index % 2 !== 0) {
                            return true;
                        }
                        return false;
                    });
                    setSelectedRowKeys1(newSelectedRowKeys);
                },
            },
        ],
    };
    return (
        <div>
            {/* 答题 */}
            <div className='headerbox'>
                <div>
                    <h3>挑战页面</h3>
                </div>
                <div>
                </div>
            </div>
            {/* table列表 */}
            <div className='tablebox'>
                {/* 挑战者 */}
                <div style={{ width: '46%' }}>
                    <p style={{ fontSize: '19px', fontWeight: '800', marginBottom: '2vw' }}>

                        挑战者
                        <span style={{ float: 'right' }}> <Button onClick={() => getData()} type="link">点击刷新</Button></span>
                    </p>
                    <Table columnTitle='全选' style={{ width: '100%', display: 'inline-block', marginRight: '5vw' }} rowSelection={rowSelection} columns={columns} dataSource={data} />
                </div>
                {/* 擂主 */}
                <div style={{ width: '46%' }}>
                    <p style={{ fontSize: '19px', fontWeight: '800', marginBottom: '2vw' }}>擂主</p>
                    <Table columnTitle='全选' style={{ width: '100%', display: 'inline-block' }} rowSelection={rowSelection1} columns={columns} dataSource={datas1} />
                </div>
            </div>
            <div style={{ float: 'right', marginRight: '10vw' }}>
                <Button onClick={currryPk} type="dashed">进行PK</Button>
            </div>
        </div>
    )
})
