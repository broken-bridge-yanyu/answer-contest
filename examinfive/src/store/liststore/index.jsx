import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { instance } from '../../service/request'
// 封装持久化的方法
const storage = {
    // 设置数据
    setstorage(key, value) {
        sessionStorage[key] = JSON.stringify(value)
    },
    // 读取数据
    getstorage(key) {
        return JSON.parse(sessionStorage[key] || '[]')
    }
}
// 异步的redux 点击触发的方法，state是传递的值
export const getList = createAsyncThunk('lists/getList', async (state, action) => {
    const response = await instance.get('/rob/clists', {
        // 接收参数传递后端，来过滤数据
        params: {
            cLists: state.champion,
            cgLists: state.challenger
        }
    })
    console.log(response, 123123);
    // 进行判断如果是200就返回值，如果不是返回传递的值
    if (response.code === 200) {
        return {
            cLists: response.championData,
            cgLists: response.challengerData
        }
    } else {
        return response
    }

})

export const editData = createAsyncThunk('lists/editData', async (state, action) => {

    const response = await instance.get('/rob/clists', {
        // 接收参数传递后端，来过滤数据
        params: {
            _id: state.id,
            fraction: state.val
        }
    })
    if (response.code === 200) {

        return response.data
    } else {
        return '错误了'
    }

})

const todosSlice = createSlice({
    name: 'lists',
    // 保存数据的地方
    initialState: {
        cLists: storage.getstorage('cLists'),
        cgLists: storage.getstorage('cgLists'),

    },
    // 同步方法的触发
    reducers: {


    },
    // 进行异步数据的获取
    extraReducers: builder => {
        builder
            .addCase(getList.pending, state => {
                console.log('进行中');
            })
            .addCase(getList.fulfilled, (state, { payload }) => {
                console.log('成功');
                storage.setstorage('cLists', payload.cLists)
                storage.setstorage('cgLists', payload.cgLists)

            })
            .addCase(getList.rejected, (state, err) => {
                console.log('失败', err);
            })
        builder
            .addCase(editData.pending, state => {
                console.log('进行中');
            })
            .addCase(editData.fulfilled, (state, { payload }) => {
                const findindex = state.cLists.findIndex(item => item._id === payload._id);
                const index2 = state.cgLists.findIndex(item => item._id === payload._id);
                if (findindex !== -1) {
                    // 如果找到匹配的对象，更新数组中的该对象
                    state.cLists[findindex] = payload;
                }
                storage.setstorage('cLists', state.cLists)
                if (index2 !== -1) {
                    // 如果找到匹配的对象，更新数组中的该对象
                    state.cgLists[index2] = payload;
                }
                storage.setstorage('cgLists', state.cgLists)
                console.log('成功');


            })
            .addCase(editData.rejected, (state, err) => {
                console.log('失败', err);
            })

    }



})
// 传递的方法
export const { lists } = todosSlice.actions
// 抛出的方法
export default todosSlice;