import { configureStore } from '@reduxjs/toolkit'
import listsReducer from './liststore'


const store = configureStore({
    reducer: {
        lists: listsReducer.reducer,

    }
})

export default store;