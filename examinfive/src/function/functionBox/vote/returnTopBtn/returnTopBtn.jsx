import React from 'react'
import { useNavigate } from 'react-router-dom';
import './returnTopBtn.css'
import { Button } from 'antd';

export default function ReturnTopBtn({ pathNavigate }) {
    const navigate = useNavigate();
    const returnTop = () => {
        navigate(pathNavigate)
    }
    return (
        <div className='returnTop' >
            <Button className='returnBtn' type="primary" onClick={returnTop}>
                <span className='iconfont icon-zuojiantou'></span>返回
            </Button>
        </div>
    )
}
