import { instance } from './request'
import { getUserList } from './modules/user'
import { LoginAPI } from './modules/Login'//登录
import { GetVoteApi, DeleteVoteApi, PostVoteApi, PutVoteApi } from './modules/vote'//投票
import { GetCollegeApi, DeleteCollegeApi, PostCollegeApi } from './modules/college' // 学院管理
import { GetParticipantApi, DeleteParticipantApi, PostParticipantApi } from './modules/participant'//参赛人员


const navListGet = async (url) => {
    let res = await instance.get(url)
    return res
}

export {
    //登录
    LoginAPI,
    //导航
    navListGet,
    getUserList,
    //投票接口
    GetVoteApi,
    DeleteVoteApi,
    PostVoteApi,
    PutVoteApi,
    //学院管理
    GetCollegeApi,
    DeleteCollegeApi,
    PostCollegeApi,
    //参赛人员
    GetParticipantApi,
    DeleteParticipantApi,
    PostParticipantApi
}