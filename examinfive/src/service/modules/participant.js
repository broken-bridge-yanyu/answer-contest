import { instance } from '../request'

const GetParticipantApi = async (url) => {
    let res = await instance.get(url)
    return res
}
const DeleteParticipantApi = async (url) => {
    let res = await instance.delete(url)
    return res
}

const PostParticipantApi = async (url, data) => {
    let res = await instance.post(url, data)
    return res
}

export { GetParticipantApi, DeleteParticipantApi, PostParticipantApi }
