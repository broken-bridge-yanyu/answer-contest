import { instance } from '../request'

const GetCollegeApi = async (url) => {
    let res = await instance.get(url)
    return res
}
const DeleteCollegeApi = async (url) => {
    let res = await instance.delete(url)
    return res
}

const PostCollegeApi = async (url, data) => {
    let res = await instance.post(url, data)
    return res
}

export { GetCollegeApi, DeleteCollegeApi, PostCollegeApi }
