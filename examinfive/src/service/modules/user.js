import { instance } from '../request.js'

export function getUserList() {
    return instance.get('/')
}