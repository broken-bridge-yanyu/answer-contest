import { instance } from '../request'

const GetVoteApi = async (url) => {
    let res = await instance.get(url)
    return res
}
const DeleteVoteApi = async (url) => {
    let res = await instance.delete(url)
    return res
}
const PostVoteApi = async (url, data) => {
    let res = await instance.post(url, data)
    return res
}
const PutVoteApi = async (url) => {
    let res = await instance.put(url)
    return res
}

export { GetVoteApi, DeleteVoteApi, PostVoteApi, PutVoteApi }
