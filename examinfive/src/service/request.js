import axios from 'axios'
// import {useNavigate} from "react-router-dom"
//接口访问
let instance = axios.create({
    baseURL: 'https://caiyouyou.fun/',
    timeout: 50000,
})
//图片路径访问
let imgUrl = 'http://localhost:7321/'
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 把token加入请求头中, 不需要可以删除下面4句代码
    const accessToken = localStorage.getItem('accessToken')
    const refreshToken = localStorage.getItem('refreshToken')
    if (accessToken && refreshToken) {
        config.headers.Authorization = accessToken
        config.headers.refreshToken = refreshToken
    }
    return config
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
})
instance.interceptors.response.use(
    function (response) {
        // const router=useNavigate()
        let newauthorization = response.data.newauthorization
        if (newauthorization) {
            console.log(newauthorization, 'newauthorization');
            delete response.data.newauthorization
            localStorage.setItem("accessToken", newauthorization)
        }
        if (response.data.code === 403) {
            localStorage.clear()
            // router("/login")
            window.location.hash = "#/login"
        }
        return response.data
    },
    function (error) {
        // 对响应错误做点什么
        return Promise.reject(error)
    }
)
export { instance, imgUrl }