import "./App.css"
import React from 'react'
import routes from "./router";
import { useRoutes } from "react-router-dom";
export default function App() {
  const router = useRoutes(routes)
  return (
    <div className="App">
      {router}
    </div>
  )
}

