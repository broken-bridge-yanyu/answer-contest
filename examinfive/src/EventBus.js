import EventEmitter from 'eventemitter3'
// 创建 EventBus 实例
const eventBus = new EventEmitter();

export default eventBus;