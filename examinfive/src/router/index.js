import { Navigate } from 'react-router-dom'
import Home from '../components/home'
import Login from '../components/login'
import Homepage from '../components/home/homepage'//首页
import Participant from '../components/home/participant'//参赛人员
import Answer from '../components/home/answer'//比赛简答题
import Raceto from '../components/home/raceto'//比赛抢答页
import Community from '../components/home/community/community'//社区
import Challenge from '../components/home/challenge/challenge'//自我挑战
import Question from '../components/home/challenge/question'//比赛选择题
import Fill from '../components/home/challenge/fill'//比赛天空题
import ShootingStar from '../components/home/shootingStar' //星火大模型
import Personalcenter from '../components/personalcenter/personalcenter'//个人中心
import Jieshao from '../components/personalcenter/jieshao/index'
import Shoucang from '../components/personalcenter/shoucang/index'


import Vote from '../components/home/vote'//投票
import AddLatestActivity from '../components/home/vote/addLatestActivity'//活动添加
import StateInquire from '../components/home/vote/stateInquire'
import SingleQuery from '../components/home/vote/singleQuery'
import VotingPage from '../components/home/vote/votingPage'//投票界面
import VotingInterface from '../components/home/vote/VotingInterface'//投票数据动态显示
import SucceedPage from '../components/home/vote/votingPage/succeedPage'

import Index from '../components/home/index'
import Add from '../components/home/participant/add'
// import Update from '../components/home/update'
import Rob from '../components/rob'
import RouterAuth from "./Authrouter"


//权限管理
import CollegeManagement from '../components/home/authorityManagement/collegeManagement/collegeManagement'//学院管理

const routes = [
  {
    path: '/',
    element: <Navigate to='/login'></Navigate>
  },
  {
    path: '/login',//登录
    element: <Login></Login>,
  },
  {
    path: '/rob',
    element: <RouterAuth><Rob></Rob></RouterAuth>
  },
  {
    path: '/personalcenter',
    element: <Personalcenter></Personalcenter>,
    children:[
      {
        path: 'jieshao',
        element: <Jieshao/>
      },
      {
        path: 'shoucang',
        element: <Shoucang/>
      },
    ]
  },
  {
    path: '/home',
    element: <RouterAuth><Home></Home></RouterAuth>,
    children: [
      {
        path: '/home',
        element: <Navigate to='/home/homepage' replace />
      },
      {
        path: '/home/homepage',
        element: <Homepage></Homepage>
      },
      {
        path: '/home/participant',
        element: <Participant></Participant>
      },
      {
        path: '/home/participant/index',
        element: <Index></Index>
      },
      {
        path: '/home/participant/add',
        element: <Add></Add>
      },
      // {
      //   path: '/home/participant/update',
      //   element: <Update></Update>
      // },
      {
        path: '/home/answer',
        element: <Answer></Answer>
      },
      {
        path: '/home/raceto',
        element: <Raceto></Raceto>
      },
      {
        path: '/home/shootingStar',
        element: <ShootingStar></ShootingStar>
      },// 星火大模型
      {
        path: '/home/vote',//活动投票页
        element: <Vote></Vote>
      },
      {
        path: '/home/AddLatestActivity',
        element: <AddLatestActivity />
      },
      {
        path: '/home/StateInquire',
        element: <StateInquire />
      },
      {
        path: '/home/community',
        element: <Community></Community>
      },
      {
        path: '/home/challenge',
        element: <Challenge></Challenge>
      },
      {
        path: '/home/challenge/question',
        element: <Question></Question>
      },
      {
        path: '/home/challenge/fill',
        element: <Fill></Fill>
      },
      //权限管理CollegeManagement
      {//学院管理
        path: '/home/collegeManagement',
        element: <CollegeManagement></CollegeManagement>
      },
    ],
  },
  {
    path: '/SingleQuery',
    element: <RouterAuth><SingleQuery /></RouterAuth>
  },
  {
    path: '/VotingPage',
    element: <VotingPage />
  },
  {
    path: '/VotingInterface',
    element: <VotingInterface />
  },
  {
    path: '/SucceedPage',
    element: <SucceedPage />
  },
]
export default routes;