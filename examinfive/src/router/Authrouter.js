import { Navigate } from "react-router-dom";

function GetToken(){
    return localStorage.getItem("accessToken");
}
function RouterAuth({children}){
    const token=GetToken();
    if(token){
        return <>{children}</>
    }else{
        return <Navigate to="/login"></Navigate>
    }
}
export default RouterAuth