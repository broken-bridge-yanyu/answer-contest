import React from "react";
import { useLocation, Navigate } from "react-router-dom";

function AuthRouter(props) {
  const token = localStorage.getItem("accessToken");
  const { pathname } = useLocation();
  if (pathname === "/login") {
    return props.children;
  }
  if (!token) {
    return <Navigate to="/login"></Navigate>;
  } else {
    return props.children;
  }
}

export default AuthRouter;
