const mongoose = require('../db')

const participantSchema = new mongoose.Schema({
    //基本信息
    activityCover: String,//封面
    activityTitle: String,//活动标题
    createTime: Date,//活动开始时间
    overTime: Date,//结束时间
    describe: String,//活动描述
    channelState: {//投票渠道状态
        type: Boolean,
        default: true
    },
    contestant: [//参赛选手/小组信息
        {
            name: String,//参赛选手/小组名称
            uuid: String,//uuid
            state: {//状态
                type: Boolean,
                default: true
            },
            votes: {//票数
                type: Number,
                default: 0,
            },
            portrait: String,//参赛者/小组的图片
            personnel: {
                type: [mongoose.Types.ObjectId],
                ref: 'robuser'
            },
            slogan: String
        }
    ],
    //参与人数
    participant: {//参与人数
        type: Number,
        default: 0,
    },
    establishTime: {//创建时间
        type: Date,
        default: new Date().getTime()
    },
    // QRcodeStyle://二维码样式
})
const participantModel = mongoose.model('participant', participantSchema)

module.exports = {
    participantModel,
}