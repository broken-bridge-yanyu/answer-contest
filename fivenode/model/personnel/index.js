const mongoose = require('../db')

var Schema = mongoose.Schema

var useSchema = new Schema({
    name: String,
    age: String,
    gender: String,
    stage: String,
    className: String,
    college: String,
    description: String,
    files: String,
    fraction: {
        type: Number,
        default: 0
    },
    RobFraction: {
        type: Number,
        default: 0
    }


})
var userModel = mongoose.model('useradd', useSchema, 'useradd')
module.exports = {
    userModel
}