const mongoose = require('./db')

//学院/阶段/班级
const collegeSchema = new mongoose.Schema({
    collegeName: String,
    level: {
        type: Number,
        default: 0
    },
    c_id: {//子级阶段和班级
        type: mongoose.Types.ObjectId,
        ref: 'college'
    }
})
const collegeModel = mongoose.model('college', collegeSchema)

const userSchema = new mongoose.Schema({
    username: String,//人名
    password: String,//密码
    email: String,//邮箱
    phone: String,//手机号
    flag: {
        type: Boolean,
        default: false
    },
    age: String,//年龄
    gender: String,
    college: {//学院、阶段、班级
        type: [mongoose.Types.ObjectId],
        ref: 'college'
    },
    description: String,
    files: String,
    fraction: {//答题分数
        type: Number,
        default: 0
    },
    RobFraction: {//抢答分数
        type: Number,
        default: 0
    }
    // },
    // {
    //     // 获取两个字段
    //     timeStamps: true
})
const usermodel = mongoose.model('robuser', userSchema, 'robuser')


const shortQuestionSchema = new mongoose.Schema({
    title: String,
    types: {
        type: Number,
        default: 0
    },
    info: {
        type: Array,
        default: []
    },
    flag: {
        type: Boolean,
        default: false
    }
})

const shortQuestionmodel = mongoose.model('shortQuestion', shortQuestionSchema, 'shortQuestion')



module.exports = {
    usermodel,
    shortQuestionmodel,
    collegeModel,//学院/阶段/班级
}