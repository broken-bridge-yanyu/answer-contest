const mongoose = require('./db')

const navigationBarSchema = new mongoose.Schema({
    navName: String,
    level: Number,
    n_id: {
        type: mongoose.Types.ObjectId,
        ref: 'navigationBar'
    },
    path: String
})
const navigationBarModel = mongoose.model('navigationBar', navigationBarSchema)


module.exports = {
    navigationBarModel
}