const mongoose = require("../db");
let Schema = mongoose.Schema;
// 用户模型
let userSchema = new Schema({
  name: String,
  password: String,
  img: String,
  state: {
    type: Boolean,
    default: true,
  },
  role_id: {
    type: mongoose.Types.ObjectId,
    ref: "roles",
  },
});
// 角色模型
let roleSchema = new Schema({
  name: String,
});
//路由模型
let routes = new Schema({
  name: String,
  path: String,
  parent_id: {
    type: mongoose.Types.ObjectId,
    ref: "routes",
    default: null,
  },
  computed:{
    type:String,
    default:null
  },
  menuname:{
    type:String,
    default:null
  }
});
// 权限模型 用户的路由权限
const AuthoritySchema = new Schema({
  role_id: {
    type: mongoose.Types.ObjectId,
    ref: "roles",
  },
  routes_id: {
    type: mongoose.Types.ObjectId,
    ref: "routes",
  },
});
const Usermodel = mongoose.model("users", userSchema, "users");
const Rolemodel = mongoose.model("roles", roleSchema, "roles");
const Routesmodel = mongoose.model("routes", routes, "routes");
const Authoritymodel = mongoose.model(
  "authority",
  AuthoritySchema,
  "authority"
);
module.exports = {
  Usermodel,
  Rolemodel,
  Routesmodel,
  Authoritymodel,
};
