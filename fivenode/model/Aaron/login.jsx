
var express = require('express');
var router = express.Router();

const bcryptjs = require("bcryptjs"); //引入 bcryptjs 进行加密解密
const jwt=require('jsonwebtoken')
const SECRET="panpan"
let {
  Usermodel,
  Rolemodel,
  Routesmodel,
  Authoritymodel,
} = require("../modules/user");
// router.get('/', async (req, res) => {
//   await req.render('index', {
//     title: 'Hello Koa 2!'
//   })
// })
async function toTree(data) {
  // 删除 所有 children,以防止多次调用
  data.forEach(function (item) {
      delete item.children;
  });

  // 将数据存储为 以 id 为 KEY 的 map 索引数据列
  var map = {};
  data.forEach(function (item) {
      map[item._id] = item;
  });
//        console.log(map);

  var val = [];
  data.forEach(function (item) {

      // 以当前遍历项，的pid,去map对象中找到索引的id
      var parent = map[item.parent_id];

      // 好绕啊，如果找到索引，那么说明此项不在顶级当中,那么需要把此项添加到，他对应的父级中
      if (parent) {

          (parent.children || ( parent.children = [] )).push(item);

      } else {
          //如果没有在map中找到对应的索引ID,那么直接把 当前的item添加到 val结果集中，作为顶级
          val.push(item);
      }
  });

  return val;
}




router.get("/bcryptjs", async (req, res) => {
  let data = "123456";

  let bcryptjsd = bcryptjs.hashSync(data);
  // console.log(res());
  let password = await bcryptjs.h(data, bcryptjsd);

  res.send({
    bcryptjsd,
    password,
  })
});
router.post("/adduser", async (req, res) => {
  let newauthorization= req.headers.newauthorization

  let body = req.request.body;
  body.password = bcryptjs.hashSync(body.password);
  let user = await Usermodel.findOne({ name: body.name });
  if (user) {
    res.send({
      code: 400,
      msg: "用户名已存在",
    })
  } else {
    let data = await Usermodel.create(body);
    res.send({
      body,
      code: 200,
      msg: "添加用户成功",
      newauthorization
    });
  }
});
router.post("/addrole", async (req, res) => {
  let newauthorization= req.headers.newauthorization
  
  let body = req.body;
  let data = await Rolemodel.create(body);
  res.send(
    {
      data,
      code: 200,
      msg: "添加成功",
      newauthorization
    }
  );
});
router.post("/addroutes", async (req, res) => {
  let body = req.body;
  console.log(body);
  let newauthorization= req.headers.newauthorization

  let data = await Routesmodel.create(body);
  res.send({
    newauthorization,
    data,
    code: 200,
    msg: "添加成功",
  });
});
router.post("/addAuthority", async (req, res) => {
  let newauthorization= req.headers.newauthorization
  let body = req.body.list;
  let data = await Authoritymodel.create(body);
  req.send({
    newauthorization,
    data,
    code: 200,
    msg: "添加成功",
  });
});
router.post("/login", async (req, res) => {
  console.log(111);
  let name = req.body.name;
  let user = await Usermodel.findOne({ name: name }).populate("role_id");
  let password = bcryptjs.compareSync(req.body.password, user.password);//判断密码是否一致

  if (user) {
    if (!password) {
      res.send({
        code: 401,
        msg: "密码错误",
      });
    }
    let auth = await Authoritymodel.aggregate([
      {
        $lookup: {
          from: "routes",
          localField: "routes_id",
          foreignField: "_id",
          as: "menu",
        },
      
      },
      {
        $match:{
          role_id:user.role_id._id
         }
      }
    ]);
    let authlist=auth.map(item=>item.menu[0])
    let suthli=await toTree(authlist)
    let accessToken =  jwt.sign({id:user._id }, SECRET, { expiresIn: '180s' }) 
    //验证是否登录使用的token
    let refreshToken =  jwt.sign({ id:user._id }, SECRET, { expiresIn: '10d' }); 
    //登录token过期后使用该token去获取
    res.send({
      user,
      suthli,
      accessToken,
      refreshToken
    });
  } else {
    req.send({
      code: 401,
      msg: "不存在",
    });
  }
});
router.get('/users',async(req,res)=>{
  res.send({
    data:"xxx"
  })
})

module.exports = router;
