// aaa.js
const express = require('express');
const router = express.Router();
const {
  usermodel,
  collegeModel,//学院
} = require('../model/rob'); // 请替换为您的模型路径

// 获取数据列表
router.get('/list', async (req, res) => {
  try {
    const data = await usermodel.find().populate('college'); // 使用您的模型获取数据
    res.json({ data });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});
// 添加数据
router.post('/add', async (req, res) => {
  try {
    // 使用 req.body 中的数据创建新记录
    const newData = await usermodel.create(req.body);
    res.json({ newData });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// 添加excel数据
router.post('/addExcel', async (req, res) => {
  const body = req.body
  body.map(item => {
    delete item.key
    return item
  })
  try {
    await usermodel.create(body);
    res.json({ code: 200, message: '数据已成功添加' });
  } catch (error) {
    res.send({ code: 500, err: '添加失败' })
  }
})
router.post('/update', async (req, res) => {
  try {
    // console.log(req.body);
    const { _id, ...updatedData } = req.body;
    // 在这里执行数据库更新操作
    await usermodel.findByIdAndUpdate(_id, updatedData);
    res.json({ message: '数据已成功更新' });
  } catch (error) {
    console.error('更新数据时出现错误：', error);
    res.status(500).json({ error: '内部服务器错误' });
  }
});
router.put('/edit/:id', async (req, res) => {
  console.log(req.body);
  let _id = req.params.id;
  console.log(_id);
  try {
    // 在这里执行数据库更新操作
    await usermodel.findByIdAndUpdate(_id, req.body);
    res.json({ message: '数据已成功更新', code: 200 });
  } catch (error) {
    console.error('更新数据时出现错误：', error);
    res.status(500).json({ error: '内部服务器错误' });
  }
})
// 删除数据
router.get('/del', async (req, res) => {
  try {
    // 使用 req.query.id 删除记录
    await usermodel.findByIdAndDelete(req.query.id);
    const data = await usermodel.find(); // 更新数据列表
    res.json({ data });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

//学院管理列表
router.get('/collegeList', async (req, res) => {
  let datas = await collegeModel.find().lean()
  let data = []
  let dataObj = {}
  datas.forEach(item => {
    dataObj[item._id] = item
  })
  datas.forEach(item => {
    if (!item['c_id']) {
      data.push(item)
    } else {
      if (!dataObj[item['c_id']]['children']) {//没有children属性
        //为该对象添加一个children属性
        dataObj[item['c_id']]['children'] = []
      }
      dataObj[item['c_id']]['children'].push(item)
    }
  })
  res.send({ code: 200, msg: '成功', data })
})
//学院管理删除
router.delete('/collegeDel/:id', async (req, res) => {
  try {
    let id = req.params.id
    let children = await collegeModel.find({ c_id: id })
    await collegeModel.find({ _id: id }).deleteMany()//父级删除
    for (const child of children) {//采用递归
      await collegeModel.find({ c_id: child._id }).deleteMany()//删除子级下面的子级
      await collegeModel.find({ _id: child._id }).deleteMany()//删除父级下面的子级
    }
    res.send({ code: 200, msg: '删除成功' })
  } catch (error) {
    console.error(error);
    res.send({ code: 400, msg: '删除失败' })
  }
})
//学院添加
router.post('/collegeAdd', async (req, res) => {
  try {
    if (req.body.collegeAdd?.level === 1) {
      await collegeModel.create({ c_id: req.body.collegeAdd.c_id, collegeName: req.body.addChilder, level: req.body.collegeAdd.level })
      res.send({ code: 200, msg: '阶段添加成功' })
    } else if (req.body.collegeAdd?.level === 2) {
      await collegeModel.create({ c_id: req.body.collegeAdd.c_id, collegeName: req.body.addChilder, level: req.body.collegeAdd.level })
      res.send({ code: 200, msg: '班级添加成功' })
    } else {
      let { collegeName } = req.body
      await collegeModel.create({ collegeName })
      res.send({ code: 200, msg: '学院添加成功' })
    }
  } catch (error) {
    console.error(error);
    res.send({ code: 400, msg: '学院添加失败' })
  }
})
//学院/班级数据修改
router.post('/collegeUpd/:id', async (req, res) => {
  try {
    await collegeModel.findOne({ _id: req.params.id }).updateOne({ collegeName: req.body.data })
    res.send({ code: 200, msg: '修改成功' })
  } catch (error) {
    console.error(error);
    res.send({ code: 400, msg: '修改失败' })
  }
})

module.exports = router;
