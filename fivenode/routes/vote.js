var express = require('express');
var router = express.Router();
var { participantModel } = require('../model/vote/index')
var multiparty = require('multiparty')
var fs = require('fs');


//图片上传
router.post('/imageUpload/:id', (req, res) => {
    let userId = req.params.id
    let currenDate = new Date()
    let year = currenDate.getFullYear()
    let month = String(currenDate.getMonth() + 1).padStart(2, '0');
    let day = String(currenDate.getDate()).padStart(2, '0');
    let path = `upload/contestant/${userId}Img/${year}-${month}-${day}`
    function form() {
        let forms = new multiparty.Form({ uploadDir: path })
        forms.parse(req, (err, formData, ImageData) => res.send({ path: ImageData.avatar[0].path }))
    }
    if (fs.existsSync(path)) form()
    else {
        fs.mkdir(path, { recursive: true }, (err) => {
            if (err) return console.log('目录创建失败！');
            form()
        })
    }
})

//图片删除
router.post('/imageDelete', (req, res) => {
    if (req.body.path) {
        fs.unlinkSync(req.body?.path)
        res.send({ code: 200, msg: '图片删除成功' })
    }
})

//添加活动
router.post('/addParticipant', async (req, res) => {
    try {
        let data = await participantModel.create({
            activityCover: req.body.activityCover,
            activityTitle: req.body.activityTitle,
            createTime: req.body.createTime,
            overTime: req.body.overTime,
            describe: req.body.describe ? req.body.describe : '没有留下任何痕迹！',
            contestant: req.body.conteStant.map(item => ({
                uuid: item.id,
                name: item.name,
                personnel: item.personnel,
                portrait: item.portrait,
                slogan: item.slogan
            })),
        })
        res.send({ code: 200, msg: '添加成功', data })
    } catch (error) {
        console.error(error);
        res.send({ code: 500, msg: '添加失败！' })
    }
});
//活动页面的数据
router.get('/listParticipant', async (req, res) => {
    try {
        let data = await participantModel.find()
        res.send({ code: 200, msg: '成功', data })
    } catch (error) {
        console.error(error);
        res.send({ code: 500, msg: '获取数据失败' })
    }
})
//活动状态修改
router.put('/activeStateUpdata/:id/:state', async (req, res) => {
    await participantModel.findOne({ _id: req.params.id }).updateOne({ channelState: req.params.state })
    let data = await participantModel.findOne({ _id: req.params.id })
    // @ts-ignore
    res.send({ code: 200, msg: '成功', state: data.channelState })
})
//其中一个活动修改
router.post('/activeUpdata/:id', async (req, res) => {
    console.log(req.body);
    await participantModel.findOne({ _id: req.params.id }).updateMany({
        activityCover: req.body.activityCover,
        activityTitle: req.body.activityTitle,
        createTime: req.body.createTime,
        overTime: req.body.overTime,
        describe: req.body.describe,
        contestant: req.body.contestant.map(item => ({
            name: item.name,
            uuid: item.uuid,
            portrait: item.portrait,
            personnel: item.personnel,
            slogan: item.slogan,
        })),
    })
    res.send({ code: 200, msg: '修改成功' })
})

//活动删除某行
router.delete('/delParticipant/:id', async (req, res) => {
    try {
        await participantModel.findOne({ _id: req.params.id }).deleteOne()
        res.send({ code: 200, msg: '删除成功' })
    } catch (error) {
        console.error(error);
        res.send({ code: 500, msg: '删除失败' })
    }
})
router.get('/listParticipants/:id', async (req, res) => {
    try {
        let data = await participantModel.find({ _id: req.params.id })
        res.send({ code: 200, data, msg: '获取成功' })
    } catch (error) {
        console.error(error);
        res.send({ code: 500, msg: '获取失败' })
    }
})

//某个活动
router.post('/voteAction/:id', async (req, res) => {
    try {
        const participant = await participantModel.findOne({ _id: req.params.id });
        // 假设 participant 存在 contestant 数组中的某个元素需要更新，通过版本号进行锁定
        const contestantIndex = participant.contestant.findIndex(contestant => contestant._id == req.body.id._id);
        if (contestantIndex !== -1 && participant.__v === req.body.dataList[0].__v) {
            await participantModel.updateOne(
                { _id: req.params.id, "contestant._id": req.body.id._id, __v: req.body.dataList[0].__v },
                {
                    $set: { "contestant.$.votes": req.body.id.votes + 1 },
                    $inc: { __v: 1 } // 增加版本号
                }
            );
            res.send({ code: 200, msg: '投票成功！' });
        } else {
            res.send({ code: 500, msg: '数据已被修改，请刷新后重试！' });
        }
    } catch (error) {
        res.status(500).send({ code: 500, msg: '发生错误，请重试！', error: error.message });
    }
})


module.exports = router;
