var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken')
const SECRET = 'panpan'
const users = [
  {
    username: "admin",
    password: 'admin',
  }
]
router.post('/login', async (req, res) => {
  let data = req.body
  console.log(data);
  let user = users.find(c => {
    if (c.username == data.username && c.password == data.password) {
      return c
    }
  })
  if (user) {
    let accessToken = jwt.sign({ ...user }, SECRET, { expiresIn: '30s' }) //验证是否登录使用的token
    let refreshToken = jwt.sign({ ...user }, SECRET, { expiresIn: '10d' }); //登录token过期后使用该token去获取
    res.send({
      code: 200,
      msg: '登录成功',
      user,
      accessToken,
      refreshToken
    })
  }
});
module.exports = router;
