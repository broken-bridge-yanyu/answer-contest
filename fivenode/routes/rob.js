var express = require('express');
var router = express.Router();

var { usermodel, shortQuestionmodel } = require('../model/rob')


router.get('/roblist', async (req, res) => {
    // 分数的加减
    // let cur_index = req.query.cur_index;
    // 提取分数前五名的数据
    let robdata1 = await usermodel.find().sort({ fraction: -1 }).limit(5).populate('college');
    // 提取去过滤掉前五条的数据
    let robdata = await usermodel.find().sort({ fraction: -1 }).skip(5).populate('college');
    // 随机题目的数据
    let question = await shortQuestionmodel.find()
    let question2 = await shortQuestionmodel.find()
    // 创建一个set实例  可以存储唯一的数据这个搜索出来的数据不会重复
    var uniqueData = new Set();
    // 生成随机数据
    var maxDataCount = Math.min(5, robdata.length);
    // 用来随机选择数据的下标不会超过这个数组的最大长度
    while (uniqueData.size < maxDataCount) {
        var op = Math.floor(Math.random() * robdata.length);
        uniqueData.add(robdata[op]);
    }
    var uniqueData2 = new Set();
    var maxDataCount2 = Math.min(1, question.length);
    while (uniqueData2.size < maxDataCount2) {
        var op = Math.floor(Math.random() * question.length);
        uniqueData2.add(question[op]);
    }
    // uniqueData2.add(question[Number(cur_index)])
    question = Array.from(uniqueData2);
    // 转化为数组
    robdata = Array.from(uniqueData);
    console.log(robdata, '挑战者');
    res.send({
        code: 200,
        data: robdata,
        data1: robdata1,
        question,
        question2
    });
});
// // !更改抢答的分数
// router.get('/updateScore', async (req, res) => {
//     const { _id, RobFraction } = req.query;
//     if (RobFraction != '' && RobFraction != undefined) {
//         await usermodel.updateOne({ _id }, { RobFraction });
//         let data = await usermodel.findOne({ _id });

//         res.send({
//             code: 200,
//             msg: '修改成功',
//             data,
//         });
//     }
// });

router.get('/clists', async (req, res) => {
    // 传回来的数组进行数据的查询
    const { cLists, cgLists } = req.query
    const { _id, fraction } = req.query
    if (cLists != '' && cLists != undefined) {
        // 获取擂主的数据
        let championData = await usermodel.find({ _id: { $in: cLists } }).populate('college')
        // 获取挑战者的数据
        let challengerData = await usermodel.find({ _id: { $in: cgLists } }).populate('college')
        console.log(championData);
        res.send({
            code: 200,
            championData,
            challengerData
        })
    }
    if (fraction != '' && fraction != undefined) {
        await usermodel.updateOne({ _id }, { fraction })
        let data = await usermodel.findOne({ _id }).populate('college')
        res.send({
            code: 200,
            msg: '修改成功',
            data
        })
    }
})

module.exports = router;
