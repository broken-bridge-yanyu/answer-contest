var express = require('express');
var router = express.Router();
var {
    navigationBarModel
} = require('../model/model')


router.get('/', async (req, res) => {

});

router.get('/navigationBarList', async (req, res) => {
    let datas = await navigationBarModel.find().lean()
    let data = []
    let dataObj = {}
    datas.forEach(item => {
        dataObj[item._id] = item
    })
    datas.forEach(item => {
        if (!item['n_id']) {
            data.push(item)
        } else {
            if (!dataObj[item['n_id']]['children']) {
                dataObj[item['n_id']]['children'] = []
            }
            dataObj[item['n_id']]['children'].push(item)
        }
    })
    res.send({ code: 200, data })
})

module.exports = router;
