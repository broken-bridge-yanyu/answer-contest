var express = require('express');
var router = express.Router();
var { participantModel } = require('../../model/vote/index')

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express' });
});
router.get('/returnVote/:id', async (req, res) => {
    //设置响应头
    res.set({
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        Connection: 'keep-alive',
    })
    //刷新响应头，确保客户但收到响应
    res.flushHeaders();
    //向客户端发送数据
    const interval = setInterval(async () => {
        //向客户端发送默认的message事件
        const data = await participantModel.find({ _id: req.params.id }).populate('contestant')
        let datas = JSON.stringify(data)
        res.write(`data:${datas}\n\n`);
    }, 100);
    // const custom_event_interval = setInterval(() => {
    //     //向客户端发送自定义的ping事件
    //     res.write('event: ping\n');
    //     res.write(`data:${Date()}\n\n` + '我是ping');
    // }, 5000);
    //客户端关闭连接时，清除定时器
    res.on('close', () => {
        console.log('客户端关闭了与服务器的连接。', new Date().getTime());
        clearInterval(interval);
        // clearInterval(custom_event_interval)
        res.end();
    })
})
router.get('/actionVote/:id', async (req, res) => {
    //设置响应头
    res.set({
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        Connection: 'keep-alive',
    })
    //刷新响应头，确保客户但收到响应
    res.flushHeaders();
    //向客户端发送数据
    const interval = setInterval(async () => {
        //向客户端发送默认的message事件
        const data = await participantModel.find({ _id: req.params.id })
        let datas = JSON.stringify(data)
        res.write(`data:${datas}\n\n`);
    }, 100);
    // const custom_event_interval = setInterval(() => {
    //     //向客户端发送自定义的ping事件
    //     res.write('event: ping\n');
    //     res.write(`data:${Date()}\n\n` + '我是ping');
    // }, 5000);
    //客户端关闭连接时，清除定时器
    res.on('close', () => {
        console.log('客户端关闭了与服务器的连接。', new Date().getTime());
        clearInterval(interval);
        // clearInterval(custom_event_interval)
        res.end();
    })
})
module.exports = router;
