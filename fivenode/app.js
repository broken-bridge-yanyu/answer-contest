var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var homeRouter = require('./routes/home');//首页
var loginRouter = require('./routes/login');//登录、注册
var personnelRouter = require('./routes/personnel');//参赛人员名单
var voteRouter = require('./routes/vote');//投票页
var robRouter = require('./routes/rob');//抢答页
var questionRouter = require('./routes/question');//未使用
var serverSSERouter = require('./routes/server/SSE')


var cors = require('cors')
const jwt = require('./utils/jwt');// 引入jwt token工具
var app = express();
app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/upload/contestant', express.static('./upload/contestant'))



app.use('/', indexRouter);
app.use('/home', homeRouter);
app.use('/login', loginRouter);
app.use('/personnel', personnelRouter);
app.use('/vote', voteRouter);
app.use('/rob', robRouter);
app.use('/question', questionRouter);
app.use('/serverSSE', serverSSERouter);//挂载SSE接口


app.use(function (req, res, next) {
  console.log(req.url != '/server/returnVote/:id')

  // 这里知识把登陆和注册请求去掉了，其他的多有请求都需要进行token校验
  if (req.url != '/login/login' && req.url != '/login/register' && req.url != '/server/returnVote/:id') {
    //获取请求头携带的token
    let token = req.headers.authorization;
    // console.log(token);
    let refreshtoken = req.headers.refreshtoken
    //验证token是否过期
    let result = jwt.verifyToken(token);
    console.log(result, '第一次');
    // 如果验证通过就next，否则就返回登陆信息不正确(code == 100就是异常情况)
    if (result.code == 100) {
      let resrefreshtoken = jwt.verifyToken(refreshtoken)
      // console.log(resrefreshtoken);
      if (resrefreshtoken.code == 100) {
        console.log("第二次");
        res.send({
          code: 403,
          msg: 'token过期,请重新登录'
        });
      } else {
        let user = resrefreshtoken.tokenKey
        delete user.iat
        delete user.exp
        let newauthorization = jwt.generateToken(user, '30s')
        req.headers.newauthorization = newauthorization
        next()
      }
    } else {
      next()
    }
  } else {
    next();
  }
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
